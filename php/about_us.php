<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Master Piada</title>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Bitter">
    <link rel="stylesheet" type="text/css" href="../css/general.css">
    <link rel="stylesheet" type="text/css" href="../css/about_us.css">
    <link rel="stylesheet" type="text/css" href="../css/navbar.css">
    <link rel="stylesheet" type="text/css" href="../css/img.css">
    <link rel="stylesheet" type="text/css" href="../css/hovereffect.css">
    <link rel="stylesheet" type="text/css" href="../css/footer.css">
    <script src="../javascript/jquery-3.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  </head>
  <body>

    <?php
    include 'db_connection.php';
    include 'functions.php';

    sec_session_start();

    include('navbar.php');
    ?>

    <div class="container">
      <?php
      if (login_check($mysqli)) {
        check_notifications($_SESSION['username'], $mysqli);
      }
      ?>
      <div class="text-center">
        <h1>Chi siamo</h1>
      </div>
      <div class="col-sm-6">
        <div class="hovereffect">
          <img src="../img/about_us.jpg" class="img-rounded img-responsive full" alt="">
          <div class="overlay">
            <h2>Chi siamo</h2>
            <a class="info" href="./about_us.php">clicca qui</a>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <p>Master Piada è una realtà che, da oltre 10 anni, è specializzata nella produzione di piadine fresche. La nostra è una storia fatta di “passione” e di amore per un prodotto che, da sempre, ricorda il territorio, le tradizioni e lo spirito aperto e ilare della Romagna.</p>
        <p>Nel corso tempo, pur adattandoci costantemente alle esigenze e ai desideri dei nostri consumatori, abbiamo cercato di mantenere fede ai “capi saldi” della bontà del nostro prodotto. Gli impasti per noi non diventano “piadine” se prima non sono trascorse almeno 24 ore di riposo.</p>
        <p>Fin dalla nostra apertura abbiamo avuto un occhio di riguardo per gli studenti, cercando di offrire loro un servizio che possa agevolarli nella quotidianità dello studio fuori casa.</p>
      </div>
    </div>

    <div class="container">
      <hr/>
    </div>

    <div class="container">
      <div class="text-center">
        <h1>Dove siamo</h1>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <p>Puoi trovarci nel centro di Cesena, in Piazza del Popolo n. 123</p>
          <p>Per permettere agli studenti di risparmiare sui costi di spediazione, abbiamo stabilito contatti con le seguenti università:</p>
          <ul>
            <?php
            if ($stmt = $mysqli->prepare("SELECT * FROM universities")) {
              $stmt->execute();
              $stmt->bind_result($id, $name, $street, $num, $cap, $city, $deleted);
              while ($stmt->fetch()) {
            ?>
            <li><?php echo $name; ?></li>
            <?php
              }
              $stmt->close();
            }
            ?>
          </ul>
          <p>La nostra posizione ci permette, tuttavia, di raggiungere con rapidità ed efficienza qualsiasi punto di Cesena, così da consentire anche consegne fuori sede.</p>
        </div>
        <div class="col-sm-6">
          <div id="map"></div>
          <script src="../javascript/map.js"></script>
          <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDgLGqeIKaGES-Nky2-9mMTokkeqmEw-7o&callback=myMap"></script>
        </div>
      </div>
    </div>

    <div class="container">
      <hr/>
    </div>

    <div class="container">
      <div class="text-center">
        <h1>Orari di apertura</h1>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <img src="../img/clock_icon.jpg" class="img-responsive center-block" alt="Clock">
        </div>
        <div class="col-sm-6">
          <p>Abbiamo stabilito orari di apertura affinchè fossero il più congeniale possibile agli studenti che desiderino usufruire del nostro sevizio. Puoi trovarci aperti nei seguenti giorni e orari:</p>
          <ul>
            <li>Lunedì: <?php echo get_timetable_of("Lunedì", $mysqli); ?></li>
            <li>Martedì: <?php echo get_timetable_of("Martedì", $mysqli); ?></li>
            <li>Mercoledì: <?php echo get_timetable_of("Mercoledì", $mysqli); ?></li>
            <li>Giovedì: <?php echo get_timetable_of("Giovedì", $mysqli); ?></li>
            <li>Venerdì: <?php echo get_timetable_of("Venerdì", $mysqli); ?></li>
            <li>Sabato: <?php echo get_timetable_of("Sabato", $mysqli); ?></li>
            <li>Domenica: <?php echo get_timetable_of("Domenica", $mysqli); ?></li>
          </ul>
        </div>
      </div>
    </div>

    <div class="container">
      <hr/>
    </div>

    <div class="container">
      <div class="text-center">
        <h1>Il team</h1>
      </div>
      <div class="row">
        <div class="col-sm-4">
          <figure>
            <img src="../img/user_icon.png" class="img-responsive center-block" alt="Mario">
            <figcaption class="text-justify">Mario prepara gli impasti e le farciture, selezionando le materie prime più adeguate.</figcaption>
          </figure>
        </div>
        <div class="col-sm-4">
          <figure>
            <img src="../img/user_icon.png" class="img-responsive center-block" alt="Sara">
            <figcaption class="text-justify">Sara si occupa della lavorazione delle palline di impasto e della cottura.</figcaption>
          </figure>
        </div>
        <div class="col-sm-4">
          <figure>
            <img src="../img/user_icon.png" class="img-responsive center-block" alt="Luigi">
            <figcaption class="text-justify">Luigi gestisce la movimentazione della merce, sia in entrata che in uscita. Programma la produzione quotidianamente in base alle richieste dei clienti.</figcaption>
          </figure>
        </div>
      </div>
    </div>

    <?php include('../html/footer.html'); ?>

  </body>
</html>
