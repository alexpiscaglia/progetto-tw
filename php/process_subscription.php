<?php

include 'db_connection.php';
include 'functions.php';

sec_session_start();

if (isset($_POST['username'], $_POST['name'], $_POST['surname'], $_POST['gender'], $_POST['email'], $_POST['p'])) {
  if (subscribe($_POST['username'], $_POST['name'], $_POST['surname'], $_POST['gender'], $_POST['email'], $_POST['p'], $mysqli)) {
    login($_POST['username'], $_POST['p'], $mysqli);
    header('Location: ./home.php');
  } else {
    $_SESSION['error'] = "E' stato riscontrato un errore";
    header('Location: ./subscription.php');
  }
} else {
  $_SESSION['error'] = "Richiesta non valida";
  header('Location: ./subscription.php');
}

?>
