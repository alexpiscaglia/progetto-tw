<?php

include 'db_connection.php';
include 'functions.php';

sec_session_start();

$_SESSION['rev_page_num'] = 0;
$_SESSION['rev_scale'] = 10;

$stmt = $mysqli->query("SELECT COUNT(*) AS count FROM reviews");
$row = $stmt->fetch_assoc();
$_SESSION['rev_count'] = $row['count'];
$stmt->close();

header('Location: reviews.php');

?>
