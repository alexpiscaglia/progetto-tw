<?php
include 'db_connection.php';
include 'functions.php';

sec_session_start();

if (!login_check($mysqli)) {
  echo "Richiesta non valida";
  exit();
}
?>
<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Master Piada</title>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Bitter">
    <link rel="stylesheet" type="text/css" href="../css/general.css">
    <link rel="stylesheet" type="text/css" href="../css/cart.css">
    <link rel="stylesheet" type="text/css" href="../css/navbar.css">
    <link rel="stylesheet" type="text/css" href="../css/footer.css">
    <script src="../javascript/jquery-3.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  </head>
  <body>

    <?php include('../html/navbar.html') ?>

    <div class="container">
      <div class="progress">
        <div class="progress-bar progress-bar-primary progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
          100%
        </div>
      </div>
    </div>

    <?php
    if (isset($_SESSION['success'])) {
    ?>
    <div class="container">
      <div class="col-sm-6 col-sm-offset-3">
        <div class="panel panel-info">
          <div class="panel-heading">Ordine completato</div>
          <div class="panel-body">
            <p>Il tuo ordine è stato ricevuto. Numero ordine: <?php echo $_SESSION['order_id'] ?></p>
            <button type="submit" class="btn btn-primary" onclick="location.href='./home.php'">Torna alla home</button>
          </div>
        </div>
      </div>
    </div>
    <?php
    } else if (isset($_SESSION['error'])) {
    ?>
    <div class="container">
      <div class="col-sm-6 col-sm-offset-3">
        <div class="panel panel-danger">
          <div class="panel-heading">Errore</div>
          <div class="panel-body">
            <p>E' stato riscontrato un errore. Perfavore riprova.</p>
            <button type="submit" class="btn btn-danger" onclick="location.href='./table.php?type=Piadina'">Torna al menu</button>
            <button type="submit" class="btn btn-danger" onclick="location.href='./home.php'">Torna alla home</button>
          </div>
        </div>
      </div>
    </div>
    <?php
    }
    unset($_SESSION['success']);
    unset($_SESSION['error']);
    unset($_SESSION['order_id']);
    ?>

  </body>
</html>
