<?php
include 'db_connection.php';
include 'functions.php';

sec_session_start();

if (isset($_POST['mon_opening'], $_POST['mon_closing'],
          $_POST['tue_opening'], $_POST['tue_closing'],
          $_POST['wed_opening'], $_POST['wed_closing'],
          $_POST['thu_opening'], $_POST['thu_closing'],
          $_POST['fri_opening'], $_POST['fri_closing'],
          $_POST['sat_opening'], $_POST['sat_closing'],
          $_POST['sun_opening'], $_POST['sun_closing'])) {

  $week_days = array("Lunedì", "Martedì", "Mercoledì", "Giovedì", "Venerdì", "Sabato", "Domenica");
  $opening_times = array($_POST['mon_opening'], $_POST['tue_opening'], $_POST['wed_opening'], $_POST['thu_opening'], $_POST['fri_opening'], $_POST['sat_opening'], $_POST['sun_opening']);
  $closing_times = array($_POST['mon_closing'], $_POST['tue_closing'], $_POST['wed_closing'], $_POST['thu_closing'], $_POST['fri_closing'], $_POST['sat_closing'], $_POST['sun_closing']);

  for ($i = 0; $i < 7; $i++) {
    if (($opening_times[$i] == "" || $closing_times[$i] == "") && $stmt = $mysqli->prepare("UPDATE timetables SET opening_time = NULL, closing_time = NULL WHERE week_day = ?")) {
      $stmt->bind_param('s', $week_days[$i]);
      $stmt->execute();
      $stmt->close();
    } else if ($stmt = $mysqli->prepare("UPDATE timetables SET opening_time = ?, closing_time = ? WHERE week_day = ?")) {
      $stmt->bind_param('sss', $opening_times[$i], $closing_times[$i], $week_days[$i]);
      $stmt->execute();
      $stmt->close();
    } else {
      $_SESSION['error'] = "Errore del database";
      header('Location: ./admin-timetables_page.php');
    }
  }
  $_SESSION['success'] = "Orari modificati con successo";
  header('Location: ./admin-timetables_page.php');
} else {
  echo "Richiesta non valida";
}
?>
