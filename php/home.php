<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Master Piada</title>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Bitter">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../css/home.css">
    <link rel="stylesheet" type="text/css" href="../css/review.css">
    <link rel="stylesheet" type="text/css" href="../css/general.css">
    <link rel="stylesheet" type="text/css" href="../css/blogcomment.css">
    <link rel="stylesheet" type="text/css" href="../css/navbar.css">
    <link rel="stylesheet" type="text/css" href="../css/hovereffect.css">
    <link rel="stylesheet" type="text/css" href="../css/zoom.css">
    <link rel="stylesheet" type="text/css" href="../css/blogcomment.css">
    <link rel="stylesheet" type="text/css" href="../css/footer.css">
    <script src="../javascript/jquery-3.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  </head>
  <body>

    <?php
    include 'db_connection.php';
    include 'functions.php';

    sec_session_start();

    include('navbar.php');
    ?>

    <div class="container">
      <div class="jumbotron">
        <h1>Master Piada</h1>
        <p>Acquista oggi per ricevere comodamente dove vuoi tu il meglio della nostra gamma sempre fresca.</p>
        <p></p>
      </div>
    </div>

    <div class="container">
      <?php
      if (login_check($mysqli)) {
        check_notifications($_SESSION['username'], $mysqli);
      }
      ?>
      <div class="text-center">
        <h1 class="font-italic">Chi siamo</h1>
      </div>
      <div class="col-sm-6">
        <div class="hovereffect">
          <img src="../img/about_us.jpg" class="img-rounded img-responsive full" alt="">
          <div class="overlay">
            <h2>Chi siamo</h2>
            <a class="info" href="./about_us.php">clicca qui</a>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <p>Master Piada è una realtà che, da oltre 10 anni, è specializzata nella produzione di piadine fresche. La nostra è una storia fatta di “passione” e di amore per un prodotto che, da sempre, ricorda il territorio, le tradizioni e lo spirito aperto e ilare della Romagna.</p>
        <p>Nel corso tempo, pur adattandoci costantemente alle esigenze e ai desideri dei nostri consumatori, abbiamo cercato di mantenere fede ai “capi saldi” della bontà del nostro prodotto. Gli impasti per noi non diventano “piadine” se prima non sono trascorse almeno 24 ore di riposo.</p>
        <p>Fin dalla nostra apertura abbiamo avuto un occhio di riguardo per gli studenti, cercando di offrire loro un servizio che possa agevolarli nella quotidianità dello studio fuori casa.</p>
      </div>
    </div>

    <div class="container">
      <hr/>
    </div>

    <div class="container">
      <div class="text-center">
        <h1 class="font-italic">Menu</h1>
        <p>Prova la nostra gamma di prodotti: da piadine farcite a crescioni ripieni, ce n'è per tutti i gusti!</p>
      </div>
      <div class="row">
        <div class="col-sm-6 col-md-4 col-lg-3">
          <div class="thumbnail zoom">
            <a href="./table.php?type=Piadina">
              <img src="../img/piadina.jpg" alt="Piadina">
              <div class="caption text-center">
                <h3>Piadine</h3>
              </div>
            </a>
          </div>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-3">
          <div class="thumbnail zoom">
            <a href="./table.php?type=Crescione">
              <img src="../img/crescione.jpg" alt="Crescioni">
              <div class="caption text-center">
                <h3>Crescioni</h3>
              </div>
            </a>
          </div>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-3">
          <div class="thumbnail zoom">
            <a href="./table.php?type=Panino">
              <img src="../img/panino.jpg" alt="Panini">
              <div class="caption text-center">
                <h3>Panini</h3>
              </div>
            </a>
          </div>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-3">
          <div class="thumbnail zoom">
            <a href="./table.php?type=Bevanda">
              <img src="../img/bevande.jpg" alt="Bevande">
              <div class="caption text-center">
                <h3>Bevande</h3>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>

    <div class="container">
      <hr/>
    </div>

    <div class="container bootstrap snippet">
      <div class="text-center">
        <h1>Dicono di noi</h1>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="blog-comment">
				    <ul class="comments">
              <?php
              if ($stmt = $mysqli->prepare("SELECT * FROM reviews ORDER BY RAND() LIMIT 2")) {
                $stmt->execute();
                $stmt->store_result();
                $stmt->bind_result($review_id, $username, $date, $review, $rating);
                while ($stmt->fetch()) {
              ?>

              <li class="clearfix">
                <?php
                if (is_male($username, $mysqli)) {
                ?>
                <img src="../img/male_user_icon.jpg" class="avatar" alt="">
                <?php
                } else if (is_female($username, $mysqli)) {
                ?>
                <img src="../img/female_user_icon.jpg" class="avatar" alt="">
                <?php
                }
                ?>
                <div class="post-comments">
                  <p class="meta">
                    Il <strong><?php echo $date . " " . $username; ?></strong> ha detto:
                    <span class="star-container">
                      <?php
                      for ($i = 0; $i < $rating; $i++) {
                        echo "<span class='fa fa-star checked'></span>";
                      }
                      for ($i = 0; $i < 5 - $rating; $i++) {
                        echo "<span class='fa fa-star'></span>";
                      }
                      ?>
                    </span>
                  </p>
                  <p><?php echo $review ?></p>
                </div>
              </li>
              <?php
                }
                $stmt->close();
    					}
    					?>
				    </ul>
			    </div>
        </div>
      </div>
    </div>

    <?php include('../html/footer.html') ?>

  </body>
</html>
