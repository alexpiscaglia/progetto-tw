<?php
include 'db_connection.php';
include 'functions.php';

sec_session_start();

if (!admin_login_check($mysqli)) {
  echo "Richiesta non valida";
  exit();
}
?>
<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Master Piada</title>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Bitter">
    <link rel="stylesheet" type="text/css" href="../css/table.css">
    <link rel="stylesheet" type="text/css" href="../css/general.css">
    <link rel="stylesheet" type="text/css" href="../css/navbar.css">
    <script src="../javascript/jquery-3.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  </head>
  <body>

    <?php include('../html/admin_navbar.html'); ?>

    <div class="container">
      <?php check_notifications("admin", $mysqli); ?>
      <h1>Pagina amministratori</h1>
      <h2>Ordini pendenti:</h2>
      <table class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>N.</th>
            <th>Contenuto</th>
            <th>Orario consegna</th>
            <th>Nominativo</th>
            <th>Luogo consegna</th>
            <th>Pagamento</th>
            <th>Stato</th>
            <th>Azione</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $sql = "SELECT o.id, o.delivery, o.address, o.payment, m.username, m.name, m.surname, o.status
                  FROM orders o, members m
                  WHERE o.status IN ('Pendente', 'In preparazione', 'In consegna')
                  AND o.username = m.username
                  ORDER BY o.delivery, o.datetime";

          if ($stmt = $mysqli->prepare($sql)) {
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($order_id, $delivery, $address, $payment, $username, $name, $surname, $status);
            while ($stmt->fetch()) {
          ?>
          <tr>
            <td><?php echo $order_id; ?></td>
            <td>
              <?php
              $sql = "SELECT p.type, p.name, od.quantity
                      FROM orders_details od, products p
                      WHERE od.order_id = ?
                      AND od.product_id = p.id";

              if ($stmt2 = $mysqli->prepare($sql)) {
                $stmt2->bind_param('i', $order_id);
                $stmt2->execute();
                $stmt2->store_result();
                $stmt2->bind_result($type, $product_name, $quantity);
                while ($stmt2->fetch()) {
                  echo ($type == "Bevanda" ? "" : $type) . " " . $product_name . " x ". $quantity . "<br/>";
                }
                $stmt2->close();
              }
              ?>
            </td>
            <td><?php echo $delivery; ?></td>
            <td><?php echo $name . " " . $surname; ?></td>
            <td><?php echo $address; ?></td>
            <td><?php echo $payment; ?></td>
            <td><?php echo $status; ?></td>
            <td>
              <form action="process_advance_order.php" method="post">
                <input type="hidden" name="order_id" value="<?php echo $order_id; ?>">
                <input type="hidden" name="username" value="<?php echo $username; ?>">
                <input type="hidden" name="status" value="<?php echo $status; ?>">
                <input type="submit" class="btn btn-warning" value="Avanza">
              </form>
            </td>
          </tr>
          <?php
            }
            $stmt->close();
          }
          ?>
        </tbody>
      </table>
    </div>
  </body>
</html>
