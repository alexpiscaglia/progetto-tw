<?php
include 'db_connection.php';
include 'functions.php';

sec_session_start();

if (isset($_POST['comment'], $_POST['rating'])) {
  if ($_POST['comment'] == "") {
    $_SESSION['error'] = "Scrivi la recensione prima di confermare";
    header('Location: ./process_restart_review.php');
  } else if (add_review($_SESSION['username'], $_POST['comment'], $_POST['rating'], $mysqli)) {
    $_SESSION['success'] = "Recensione inserita con successo";
    header('Location: ./process_restart_review.php');
  } else {
    $_SESSION['review_err'] = "È stato riscontrato un errore";
    header('Location: ./process_restart_review.php');
  }
} else {
  $_SESSION['review_err'] = "Richiesta non valida";
  header('Location: ./process_restart_review.php');
}
?>
