<?php
include 'db_connection.php';
include 'functions.php';

sec_session_start();

if (!login_check($mysqli)) {
  $_SESSION['error'] = "Devi essere loggato per aggiungere prodotti al carrello";
  header('Location: table.php?type=Piadina');
} else {
  if (empty($_SESSION['shoppingcart'])) {
    $_SESSION['shoppingcart'] = array();
  }
  if (isset($_POST['product_id'], $_POST['quantity'])) {
    $id = $_POST['product_id'];
    $quantity = $_POST['quantity'];
    $_SESSION['shoppingcart'][$id] += $quantity;
    $_SESSION['success'] = "Prodotto aggiunto al carrello";
    header('Location: table.php?type=Piadina');
  } else {
    $_SESSION['error'] = "Richiesta non valida";
    header('Location: table.php?type=Piadina');
  }
}
?>
