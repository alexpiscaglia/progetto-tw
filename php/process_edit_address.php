<?php
include 'db_connection.php';
include 'functions.php';

sec_session_start();

if (isset($_POST['street'], $_POST['house_num'], $_POST['cap'], $_POST['city'], $_POST['info'])) {
  if ($stmt = $mysqli->prepare("SELECT * FROM addresses WHERE username = ?")) {
    $stmt->bind_param('s', $_SESSION['username']);
    $stmt->execute();
    $stmt->store_result();
    $op = $stmt->num_rows == 0 ? "insert" : "update";
    $stmt->close();
  }

  switch ($op) {
    case 'insert':
      if ($stmt = $mysqli->prepare("INSERT INTO addresses (username, street, house_num, cap, city, info) VALUES (?, ?, ?, ?, ?, ?)")) {
        $stmt->bind_param('ssisss', $_SESSION["username"], $_POST['street'], $_POST['house_num'], $_POST['cap'], $_POST['city'], $_POST['info']);
        $stmt->execute();
        $stmt->close();
        $_SESSION['success'] = "Modifica effettuata con successo";
        header('Location: ./process_restart_order_list.php');
      } else {
        $_SESSION['error'] = "Errore del database";
        header('Location: ./process_restart_order_list.php');
      }
      break;
    case 'update':
      if ($stmt = $mysqli->prepare("UPDATE addresses SET street = ?, house_num = ?, cap = ?, city = ?, info = ? WHERE username = ?")) {
        $stmt->bind_param('sissss', $_POST['street'], $_POST['house_num'], $_POST['cap'], $_POST['city'], $_POST['info'], $_SESSION["username"]);
        $stmt->execute();
        if ($stmt->affected_rows == 0) {
          $_SESSION['error'] = "È stato riscontrato un errore, riprova perfavore";
        } else {
          $_SESSION['success'] = "Modifica effettuata con successo";
        }
        $stmt->close();
        header('Location: ./process_restart_order_list.php');
      } else {
        $_SESSION['error'] = "Errore del database";
        header('Location: ./process_restart_order_list.php');
      }
      break;
    default:
      header('Location: ./process_restart_order_list.php');
      break;
  }
} else {
  echo "Richiesta non valida";
}
?>
