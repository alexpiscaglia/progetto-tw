<?php
//http://localhost:8080/phpmyadmin/tec_web/master_piada/php/home.php
function sec_session_start() {
  $session_name = 'sec_session_id';
  $secure = false;
  $httponly = true;
  ini_set('session.use_only_cookies', 1);
  $cookieParams = session_get_cookie_params();
  session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly);
  session_name($session_name);
  session_start();
  session_regenerate_id();
}

function login($username, $password, $mysqli) {
  if ($stmt = $mysqli->prepare("SELECT name, surname, gender, email, password, salt FROM members WHERE username = ? LIMIT 1")) {
    $stmt->bind_param('s', $username);
    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($name, $surname, $gender, $email, $db_password, $salt);
    $stmt->fetch();
    $password = hash('sha512', $password.$salt);
    if ($stmt->num_rows == 1) {
      if (checkbrute($username, $mysqli) == true) {
        // Account disabilitato
        return false;
      } else {
        if ($db_password == $password) {
          // Password corretta!
          $user_browser = $_SERVER['HTTP_USER_AGENT'];

          $username = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $username); // ci proteggiamo da un attacco XSS
          $_SESSION['username'] = $username;

          $name = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $name);
          $_SESSION['name'] = $name;

          $surname = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $surname);
          $_SESSION['surname'] = $surname;

          $_SESSION['login_string'] = hash('sha512', $password.$user_browser);
          // Login eseguito con successo.
          return true;
        } else {
          // Password incorretta.
          // Registriamo il tentativo fallito nel database.
          $now = time();
          $mysqli->query("INSERT INTO login_attempts (username, time) VALUES ('$username', '$now')");
          return false;
        }
      }
    } else {
      // L'utente inserito non esiste.
      return false;
    }
  }
}

function admin_login($username, $password, $mysqli) {
  if ($stmt = $mysqli->prepare("SELECT password, salt FROM admins WHERE username = ? LIMIT 1")) {
    $stmt->bind_param('s', $username);
    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($db_password, $salt);
    $stmt->fetch();
    $password = hash('sha512', $password.$salt);
    if ($stmt->num_rows == 1) {
      if ($db_password == $password) {
        // Password corretta!
        $user_browser = $_SERVER['HTTP_USER_AGENT'];

        $username = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $username); // ci proteggiamo da un attacco XSS
        $_SESSION['username'] = $username;

        $_SESSION['admin'] = "admin";

        $_SESSION['login_string'] = hash('sha512', $password.$user_browser);
        // Login eseguito con successo.
        return true;
      } else {
        return false;
      }
    } else {
      // L'utente inserito non esiste.
      return false;
    }
  }
}

function checkbrute($username, $mysqli) {
  $now = time();
  $valid_attempts = $now - (2 * 60 * 60);
  if ($stmt = $mysqli->prepare("SELECT time FROM login_attempts WHERE username = ? AND time > '$valid_attempts'")) {
    $stmt->bind_param('s', $username);
    $stmt->execute();
    $stmt->store_result();
    if($stmt->num_rows > 5) {
      return true;
    } else {
      return false;
    }
  }
}

function login_check($mysqli) {
  if (isset($_SESSION['username'], $_SESSION['name'], $_SESSION['surname'], $_SESSION['login_string'])) {
    $username = $_SESSION['username'];
    $login_string = $_SESSION['login_string'];
    $name = $_SESSION['name'];
    $surname = $_SESSION['surname'];
    $user_browser = $_SERVER['HTTP_USER_AGENT'];
    if ($stmt = $mysqli->prepare("SELECT password FROM members WHERE username = ? LIMIT 1")) {
      $stmt->bind_param('s', $username);
      $stmt->execute();
      $stmt->store_result();

      if ($stmt->num_rows == 1) {
        $stmt->bind_result($password);
        $stmt->fetch();
        $login_check = hash('sha512', $password.$user_browser);
        if ($login_check == $login_string) {
          // Login eseguito!!!!
          return true;
        } else {
          // Login non eseguito
          return false;
        }
      } else {
        // Login non eseguito
        return false;
      }
    } else {
      // Login non eseguito
      return false;
    }
  } else {
    // Login non eseguito
    return false;
  }
}

function admin_login_check($mysqli) {
  if (isset($_SESSION['username'], $_SESSION['admin'], $_SESSION['login_string'])) {
    $username = $_SESSION['username'];
    $login_string = $_SESSION['login_string'];
    $user_browser = $_SERVER['HTTP_USER_AGENT'];
    if ($stmt = $mysqli->prepare("SELECT password FROM admins WHERE username = ? LIMIT 1")) {
      $stmt->bind_param('s', $username);
      $stmt->execute();
      $stmt->store_result();
      if ($stmt->num_rows == 1) {
        $stmt->bind_result($password);
        $stmt->fetch();
        $login_check = hash('sha512', $password.$user_browser);
        if ($login_check == $login_string) {
          // Login eseguito!!!!
          return true;
        } else {
          // Login non eseguito
          return false;
        }
      } else {
        // Login non eseguito
        return false;
      }
    } else {
      // Login non eseguito
      return false;
    }
  } else {
    // Login non eseguito
    return false;
  }
}

function add_admin($username, $password, $mysqli) {
  $random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
  $password = hash('sha512', $password.$random_salt);
  if ($stmt = $mysqli->prepare("INSERT INTO admins (username, password, salt) VALUES (?, ?, ?)")) {
    if ($stmt->bind_param('sss', $username, $password, $random_salt) && $stmt->execute()) {
      return true;
    } else {
      return false;
    }
  } else {
    return false;
  }
}

function subscribe($username, $name, $surname, $gender, $email, $password, $mysqli) {
  $random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
  $password = hash('sha512', $password.$random_salt);
  if ($stmt = $mysqli->prepare("INSERT INTO members (username, name, surname, gender, email, password, salt) VALUES (?, ?, ?, ?, ?, ?, ?)")) {
    if ($stmt->bind_param('sssssss', $username, $name, $surname, $gender, $email, $password, $random_salt) && $stmt->execute()) {
      return true;
    } else {
      return false;
    }
  } else {
    return false;
  }
}

function get_timetable_of($week_day, $mysqli) {
  if ($stmt = $mysqli->prepare("SELECT * FROM timetables WHERE week_day = ? LIMIT 1")) {
    $stmt->bind_param('s', $week_day);
    $stmt->execute();
    $stmt->store_result();
    if ($stmt->num_rows == 1) {
      $stmt->bind_result($week_day, $opening_time, $closing_time);
      $stmt->fetch();
      if ($opening_time == NULL && $closing_time == NULL) {
        return "chiuso";
      } else {
        return substr($opening_time, 0, 5) . " - " . substr($closing_time, 0, 5);
      }
    } else {
      return "errore nel caricamento";
    }
  }
}

function add_review($username, $text, $rating, $mysqli) {
  if ($stmt = $mysqli->prepare("INSERT INTO reviews(username, date, text, rating) VALUES (?, ?, ?, ?)")) {
    $date = date('Y-m-d');
    if ($stmt->bind_param('sssi', $username, $date, $text, $rating) && $stmt->execute()) {
      return true;
    } else {
      return false;
    }
  } else {
    return false;
  }
}

function is_male($username, $mysqli) {
  if ($stmt = $mysqli->prepare("SELECT gender FROM members WHERE username = ? LIMIT 1")) {
    if ($stmt->bind_param('s', $username) && $stmt->execute()) {
      $stmt->store_result();
      $stmt->bind_result($gender);
      $stmt->fetch();
      return $gender == "m" ? true : false;
    } else {
      return false;
    }
  } else {
    return false;
  }
}

function is_female($username, $mysqli) {
  if ($stmt = $mysqli->prepare("SELECT gender FROM members WHERE username = ? LIMIT 1")) {
    if ($stmt->bind_param('s', $username) && $stmt->execute()) {
      $stmt->store_result();
      $stmt->bind_result($gender);
      $stmt->fetch();
      return $gender == "f" ? true : false;
    } else {
      return false;
    }
  } else {
    return false;
  }
}

function format_date($date) {
  return (DateTime::createFromFormat("Y-m-d", $date))->format("d/m/Y");
}

function create_alert($type, $msg) {
  echo '<div class="alert alert-' . $type . ' alert-dismissible fade in">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'
          . $msg .
       '</div>';
}

function insert_notification($receiver, $msg, $mysqli) {
  if ($stmt = $mysqli->prepare("INSERT INTO notifications (id, receiver, message, date, received) VALUES (null, ?, ?, NOW(), 0)")) {
    $stmt->bind_param('ss', $receiver, $msg);
    $stmt->execute();
    $stmt->close();
  }
}

function check_notifications($receiver, $mysqli) {
  if ($stmt = $mysqli->prepare("SELECT DISTINCT message, COUNT(*) AS count FROM notifications WHERE receiver = ? AND received = 0 GROUP BY message")) {
    $stmt->bind_param('s', $receiver);
    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($msg, $count);
    while ($stmt->fetch()) {
      if ($count > 0) {
        create_alert("info", $msg);
      }
    }
    $stmt->close();
  }
  if ($stmt = $mysqli->prepare("UPDATE notifications SET received = 1 WHERE receiver = ?")) {
    $stmt->bind_param('s', $receiver);
    $stmt->execute();
    $stmt->close();
  }
}

function debug_to_console($data) {
  $output = $data;
  if (is_array($output)) {
    $output = implode(', ', $output);
  }
  echo "<script>console.log('Debug: " . $output . "');</script>";
}

?>
