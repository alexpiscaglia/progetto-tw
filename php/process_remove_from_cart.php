<?php

include 'functions.php';

sec_session_start();

if (isset($_POST['product_id'])) {
  $id = $_POST['product_id'];
  unset($_SESSION['shoppingcart'][$id]);
  header('Location: cart_page_1.php');
}

?>
