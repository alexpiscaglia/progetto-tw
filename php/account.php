<?php
include 'db_connection.php';
include 'functions.php';

sec_session_start();

if (!login_check($mysqli)) {
  echo "Richiesta non valida";
  exit();
}

if (!isset($_SESSION['ord_page_num'], $_SESSION['ord_scale'], $_SESSION['ord_count'])) {
  header('Location: process_restart_order_list.php');
}
?>
<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Master Piada</title>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Bitter">
    <link rel="stylesheet" type="text/css" href="../css/general.css">
    <link rel="stylesheet" type="text/css" href="../css/account.css">
    <link rel="stylesheet" type="text/css" href="../css/navbar.css">
    <link rel="stylesheet" type="text/css" href="../css/footer.css">
    <script src="../javascript/jquery-3.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  </head>
  <body>

    <?php include('../html/navbar.html') ?>

    <div class="container">
      <h1>Il tuo account</h1>
      <?php
      check_notifications($_SESSION['username'], $mysqli);

      if (isset($_SESSION['success'])) {
        create_alert("success", $_SESSION['success']);
        unset($_SESSION['success']);
      } else if (isset($_SESSION['error'])) {
        create_alert("danger", $_SESSION['error']);
        unset($_SESSION['error']);
      }
      ?>
      <div class="col-sm-6">
        <div class="panel panel-info">
          <div class="panel-heading">Dati utente <a href="./edit_personal_data.php">modifica</a></div>
          <div class="panel-body">
            <?php
            if ($stmt = $mysqli->prepare("SELECT name, surname, gender, email FROM members WHERE username = ? LIMIT 1")) {
              $stmt->bind_param('s', $_SESSION["username"]);
              $stmt->execute();
              $stmt->store_result();
              $stmt->bind_result($name, $surname, $gender, $email);
              $stmt->fetch();
              $stmt->close();
            }
            ?>
            <p><strong>Nome:</strong> <?php echo $name; ?></p>
            <p><strong>Cognome:</strong> <?php echo $surname; ?></p>
            <p><strong>Genere:</strong> <?php echo $gender == 'm' ? "Uomo" : "Donna"; ?></p>
            <p><strong>E-mail:</strong> <?php echo $email; ?></p>
          </div>
        </div>
      </div>

      <div class="col-sm-6">
        <div class="panel panel-info">
          <div class="panel-heading">Indirizzo di spedizione standard <a href="./edit_address.php">modifica</a></div>
          <div class="panel-body">
            <?php
            if ($stmt = $mysqli->prepare("SELECT street, house_num, cap, city, info FROM addresses WHERE username = ? LIMIT 1")) {
              $stmt->bind_param('s', $_SESSION["username"]);
              $stmt->execute();
              $stmt->store_result();
              $stmt->bind_result($street, $house_num, $cap, $city, $info);
              $stmt->fetch();

              echo $_SESSION['name'] . " " . $_SESSION['surname'] . "<br/>"
                  . $street . " " . $house_num . "<br/>"
                  . $info . "<br/>"
                  . $cap . " " . $city;

              $stmt->close();
            }
            ?>
          </div>
        </div>
      </div>
    </div>

    <div class="container">
      <hr/>
    </div>

    <div class="container">
      <h2>I tuoi ordini</h2>
      <div class="col-sm-12">
        <?php
        $offset = $_SESSION['ord_page_num'] * $_SESSION['ord_scale'];
        if ($stmt = $mysqli->prepare("SELECT id, datetime, delivery, address, payment, amount FROM orders WHERE username = ? ORDER BY datetime DESC LIMIT ?,?")) {
          $stmt->bind_param('sii', $_SESSION["username"], $offset, $_SESSION['ord_scale']);
          $stmt->execute();
          $stmt->store_result();
          $stmt->bind_result($id, $datetime, $delivery, $address, $payment, $amount);
          while ($stmt->fetch()) {
            $d = strtotime($datetime);
            $date = date('d/m/Y', $d);
            $time = date('H:m:s',$d);
        ?>
        <div class="panel panel-info">
          <div class="panel-heading">Ordine n. <?php echo "<strong>" . $id . "</strong> del <strong>" . $date . "</strong> ore <strong>" . $time . "</strong>"; ?></div>
          <div class="panel-body">
            <div class="col-sm-6">
              <p><strong>Il tuo ordine comprendeva:</strong></p>
              <?php

              if ($stmt2 = $mysqli->prepare("SELECT product_id, quantity FROM orders_details WHERE order_id = ?")) {
                $stmt2->bind_param('i', $id);
                $stmt2->execute();
                $stmt2->store_result();
                $stmt2->bind_result($product_id, $quantity);
                while ($stmt2->fetch()) {
                  if ($stmt3 = $mysqli->prepare("SELECT type, name FROM products WHERE id = ?")) {
                    $stmt3->bind_param('i', $product_id);
                    $stmt3->execute();
                    $stmt3->store_result();
                    $stmt3->bind_result($type, $product_name);
                    $stmt3->fetch();
              ?>
              <p>- <?php echo ($type == "Bevanda" ? "" : $type) . " " . $product_name . " x " . $quantity; ?></p>
              <?php
                    $stmt3->close();
                  }
                }
                $stmt2->close();
              }
              ?>
            </div>
            <div class="col-sm-6">
              <p><strong>Consegna ore:</strong> <?php echo substr($delivery, 0, 5); ?></p>
              <p><strong>Indirizzo:</strong> <?php echo $address; ?></p>
              <p><strong>Totale:</strong> <?php echo sprintf("%01.2f", $amount) . " €"; ?></p>
              <p><strong>Modalità pagamento:</strong> <?php echo $payment; ?></p>
            </div>
          </div>
        </div>
        <?php
          }
          $stmt->close();
        }
        ?>
      </div>
      <ul class="pager">
        <li><a href="process_prev_order_list.php">Previous</a></li>
        <li><a href="process_next_order_list.php">Next</a></li>
      </ul>
    </div>

    <?php include('../html/footer.html') ?>

  </body>
</html>
