<?php
include 'db_connection.php';
include 'functions.php';

sec_session_start();

if (!admin_login_check($mysqli)) {
  echo "Richiesta non valida";
  exit();
}
?>
<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Master Piada</title>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Bitter">
    <link rel="stylesheet" type="text/css" href="../css/general.css">
    <link rel="stylesheet" type="text/css" href="../css/navbar.css">
    <script src="../javascript/jquery-3.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  </head>
  <body>

    <?php include('../html/admin_navbar.html'); ?>

    <div class="container">
      <?php check_notifications("admin", $mysqli); ?>
      <h1>Pagina amministratori</h1>
      <?php
      if (isset($_SESSION['success'])) {
        create_alert("success", $_SESSION['success']);
        unset($_SESSION['success']);
      } else if (isset($_SESSION['error'])) {
        create_alert("danger", $_SESSION['error']);
        unset($_SESSION['error']);
      }
      ?>
      <div class="col-sm-6 col-sm-offset-3">
        <div class="panel panel-warning">
          <div class="panel-heading">Orari</div>
          <div class="panel-body">
            <form class="form-horizontal" action="./process_edit_timetables.php" method="post">
              <div class="form-group">
                <label class="control-label col-sm-3">Lunedì:</label>
                <?php
                $timetable = get_timetable_of("Lunedì", $mysqli);
                $times = explode(" - ", $timetable);
                ?>
                <div class="col-sm-4">
                  <label for="mon-op" hidden>Apertura</label>
                  <input type="time" id="mon-op" class="form-control" name="mon_opening" value="<?php if ($times[0] != "chiuso") echo $times[0]; ?>">
                </div>
                <div class="col-sm-4">
                  <label for="mon-cl" hidden>Chiusura</label>
                  <input type="time" id="mon-cl" class="form-control" name="mon_closing" value="<?php if ($times[0] != "chiuso") echo $times[1]; ?>">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-3">Martedì:</label>
                <?php
                $timetable = get_timetable_of("Martedì", $mysqli);
                $times = explode(" - ", $timetable);
                ?>
                <div class="col-sm-4">
                  <label for="tue-op" hidden>Apertura</label>
                  <input type="time" id="tue-op" class="form-control" name="tue_opening" value="<?php if ($times[0] != "chiuso") echo $times[0]; ?>">
                </div>
                <div class="col-sm-4">
                  <label for="tue-cl" hidden>Chiusura</label>
                  <input type="time" id="tue-cl" class="form-control" name="tue_closing" value="<?php if ($times[0] != "chiuso") echo $times[1]; ?>">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-3">Mercoledì:</label>
                <?php
                $timetable = get_timetable_of("Mercoledì", $mysqli);
                $times = explode(" - ", $timetable);
                ?>
                <div class="col-sm-4">
                  <label for="wed-op" hidden>Apertura</label>
                  <input type="time" id="wed-op" class="form-control" name="wed_opening" value="<?php if ($times[0] != "chiuso") echo $times[0]; ?>">
                </div>
                <div class="col-sm-4">
                  <label for="wed-cl" hidden>Chiusura</label>
                  <input type="time" id="wed-cl" class="form-control" name="wed_closing" value="<?php if ($times[0] != "chiuso") echo $times[1]; ?>">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-3">Giovedì:</label>
                <?php
                $timetable = get_timetable_of("Giovedì", $mysqli);
                $times = explode(" - ", $timetable);
                ?>
                <div class="col-sm-4">
                  <label for="thu-op" hidden>Apertura</label>
                  <input type="time" id="thu-op" class="form-control" name="thu_opening" value="<?php if ($times[0] != "chiuso") echo $times[0]; ?>">
                </div>
                <div class="col-sm-4">
                  <label for="thu-cl" hidden>Chiusura</label>
                  <input type="time" id="thu-cl" class="form-control" name="thu_closing" value="<?php if ($times[0] != "chiuso") echo $times[1]; ?>">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-3">Venerdì:</label>
                <?php
                $timetable = get_timetable_of("Venerdì", $mysqli);
                $times = explode(" - ", $timetable);
                ?>
                <div class="col-sm-4">
                  <label for="fri-op" hidden>Apertura</label>
                  <input type="time" id="fri-op" class="form-control" name="fri_opening" value="<?php if ($times[0] != "chiuso") echo $times[0]; ?>">
                </div>
                <div class="col-sm-4">
                  <label for="fri-cl" hidden>Chiusura</label>
                  <input type="time" id="fri-cl" class="form-control" name="fri_closing" value="<?php if ($times[0] != "chiuso") echo $times[1]; ?>">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-3">Sabato:</label>
                <?php
                $timetable = get_timetable_of("Sabato", $mysqli);
                $times = explode(" - ", $timetable);
                ?>
                <div class="col-sm-4">
                  <label for="sat-op" hidden>Apertura</label>
                  <input type="time" id="sat-op" class="form-control" name="sat_opening" value="<?php if ($times[0] != "chiuso") echo $times[0]; ?>">
                </div>
                <div class="col-sm-4">
                  <label for="sat-cl" hidden>Chiusura</label>
                  <input type="time" id="sat-cl" class="form-control" name="sat_closing" value="<?php if ($times[0] != "chiuso") echo $times[1]; ?>">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-3">Domenica:</label>
                <?php
                $timetable = get_timetable_of("Domenica", $mysqli);
                $times = explode(" - ", $timetable);
                ?>
                <div class="col-sm-4">
                  <label for="sun-op" hidden>Apertura</label>
                  <input type="time" id="sun-op" class="form-control" name="sun_opening" value="<?php if ($times[0] != "chiuso") echo $times[0]; ?>">
                </div>
                <div class="col-sm-4">
                  <label for="sun-cl" hidden>Chiusura</label>
                  <input type="time" id="sun-cl" class="form-control" name="sun_closing" value="<?php if ($times[0] != "chiuso") echo $times[1]; ?>">
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-5 col-sm-8">
                  <button type="submit" class="btn btn-warning">Salva</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
