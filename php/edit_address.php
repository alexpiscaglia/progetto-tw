<?php
include 'db_connection.php';
include 'functions.php';

sec_session_start();

if (!login_check($mysqli)) {
  echo "Richiesta non valida";
  exit();
}
?>
<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Master Piada</title>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Bitter">
    <link rel="stylesheet" type="text/css" href="../css/general.css">
    <link rel="stylesheet" type="text/css" href="../css/navbar.css">
    <link rel="stylesheet" type="text/css" href="../css/footer.css">
    <script src="../javascript/jquery-3.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  </head>
  <body>

    <?php include('../html/navbar.html') ?>

    <div class="container">
      <h2>Modifica i tuoi dati</h2>
      <div class="col-sm-6 col-sm-offset-3">
        <div class="panel panel-warning">
          <div class="panel-heading">Dati utente</div>
          <div class="panel-body">
            <?php
            if ($stmt = $mysqli->prepare("SELECT street, house_num, cap, city, info FROM addresses WHERE username = ? LIMIT 1")) {
              $stmt->bind_param('s', $_SESSION["username"]);
              $stmt->execute();
              $stmt->store_result();
              $stmt->bind_result($street, $house_num, $cap, $city, $info);
              $stmt->fetch();
              $stmt->close();
            }
            ?>
            <form class="form-horizontal" action="./process_edit_address.php" method="post">
              <div class="form-group">
                <label class="control-label col-sm-3" for="street">Via:</label>
                <div class="col-sm-9">
                  <input id="street" class="form-control" type="text" name="street" value="<?php echo $street; ?>">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-3" for="house_num">Numero civico:</label>
                <div class="col-sm-9">
                  <input id="house_num" class="form-control" type="number" name="house_num" value="<?php echo $house_num; ?>">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-3" for="city">Città:</label>
                <div class="col-sm-9">
                  <select class="form-control" id="city" name="city">
                    <option selected>Cesena</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-3" for="cap">CAP:</label>
                <div class="col-sm-9">
                  <input id="cap" class="form-control" type="text" name="cap" value="<?php echo $cap; ?>">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-3" for="info">Campnello:</label>
                <div class="col-sm-9">
                  <input id="info" class="form-control" type="text" name="info" value="<?php echo $info; ?>">
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                  <button type="button" class="btn btn-warning" onclick="location.href='./process_restart_order_list.php'">Indietro</button>
                  <button type="submit" class="btn btn-warning">Modifica</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    <?php include('../html/footer.html') ?>

  </body>
</html>
