<?php

include 'db_connection.php';
include 'functions.php';

sec_session_start();

if (login_check($mysqli)) {
  $_SESSION['ord_page_num'] = 0;
  $_SESSION['ord_scale'] = 5;

  if ($stmt = $mysqli->prepare("SELECT COUNT(*) AS count FROM orders WHERE username = ?")) {
    $stmt->bind_param('s', $_SESSION['username']);
    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($count);
    $stmt->fetch();
    $_SESSION['ord_count'] = $count;
    $stmt->close();
  } else {
    $_SESSION['ord_count'] = 0;
  }
  
  header('Location: account.php');
} else {
  echo "Richiesta non valida";
}

?>
