<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="../css/form.css">
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Bitter">
    <script src="../javascript/jquery-3.2.1.min.js"></script>
    <script src="../javascript/sha512.js"></script>
    <script src="../javascript/forms.js"></script>
    <script src="../javascript/check_log.js"></script>
  </head>
  <body>
    <h1>Master Piada</h1>
    <section>
      <header>
        <h2>Esegui l'accesso!</h2>
        <h3>accedi per usufruire dei nostri servizi</h3>
      </header>
      <?php
      include 'functions.php';

      sec_session_start();

      if (isset($_SESSION['error'])) {
        echo "<p class='error-msg'>" . $_SESSION['error'] . "</p>";
        unset($_SESSION['error']);
      }
      ?>
      <form action="process_login.php" method="post" name="login_form">
        <fieldset>
          <legend><span>1</span> E-mail &amp; Password</legend>
          <ul>
            <li><label for="username">Username</label></li>
            <li><input id="username" type="text" name="username" autocomplete="on" required></li>
            <li><label for="password">Password</label></li>
            <li><input id="password" type="password" name="password" required></li>
            <li><label>Account</label></li>
            <li><input id="user" type="radio" name="account" value="user" checked><label for="user"> User</label></li>
            <li><input id="admin" type="radio" name="account" value="admin"><label for="admin"> Admin</label></li>
          </ul>
        </fieldset>
        <div>
          <input type="button" value="Login" onclick="check_log(this.form, this.form.password);">
          <input type="reset" value="Reset">
        </div>
      </form>
      <footer>Non hai ancora un account? <a href="./subscription.php">Registrati</a>.</footer>
    </section>
  </body>
</html>
