<?php
include 'db_connection.php';
include 'functions.php';

sec_session_start();

if (isset($_POST['username'], $_POST['p'], $_POST['account'])) {
  if ($_POST['account'] == "user" && login($_POST['username'], $_POST['p'], $mysqli)) {
    header('Location: ./home.php');
  } else if ($_POST['account'] == "admin" && admin_login($_POST['username'], $_POST['p'], $mysqli)) {
    header('Location: ./admin-orders_page.php');
  } else {
    $_SESSION['error'] = "Username o password errate!";
    header('Location: ./login.php');
  }
} else {
  $_SESSION['error'] = "Richiesta non valida";
  header('Location: ./login.php');
}
?>
