<?php

include 'functions.php';

sec_session_start();

if (($_SESSION['rev_page_num'] + 1) * $_SESSION['rev_scale'] < $_SESSION['rev_count']) {
  $_SESSION['rev_page_num'] = $_SESSION['rev_page_num'] + 1;
}

header('Location: reviews.php');

?>
