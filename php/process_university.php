<?php
include 'db_connection.php';
include 'functions.php';

sec_session_start();

if (isset($_POST['id'], $_POST['name'], $_POST['street'], $_POST['num'], $_POST['cap'], $_POST['city'])) {
  $deleted = isset($_POST['deleted']) ? 1 : 0;
  if ($_POST['id'] == "" && $stmt = $mysqli->prepare("INSERT INTO universities(id, name, street, num, cap, city, deleted) VALUES (null, ?, ?, ?, ?, ?, ?)")) {
    $stmt->bind_param('ssissi', $_POST['name'], $_POST['street'], $_POST['num'], $_POST['cap'], $_POST['city'], $deleted);
    $stmt->execute();
    $stmt->close();
    $_SESSION['success'] = "Università inserita con successo";
  } else if ($_POST['id'] != "" && $stmt = $mysqli->prepare("UPDATE universities SET name = ?, street = ?, num = ?, cap = ?, city = ?, deleted = ? WHERE id = ?")) {
    $stmt->bind_param('ssissii', $_POST['name'], $_POST['street'], $_POST['num'], $_POST['cap'], $_POST['city'], $deleted, $_POST['id']);
    $stmt->execute();
    $stmt->close();
    $_SESSION['success'] = "Università modificta con successo";
  } else {
    $_SESSION['error'] = "Errore del database";
    header('Location: ./admin-universities_page.php');
  }
  header('Location: ./admin-universities_page.php');
} else {
  echo "Richiesta non valida";
}
?>
