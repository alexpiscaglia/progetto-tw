<?php
include 'db_connection.php';
include 'functions.php';

sec_session_start();

if (!login_check($mysqli)) {
  echo "Richiesta non valida";
  exit();
}
?>
<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Master Piada</title>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Bitter">
    <link rel="stylesheet" type="text/css" href="../css/general.css">
    <link rel="stylesheet" type="text/css" href="../css/navbar.css">
    <link rel="stylesheet" type="text/css" href="../css/footer.css">
    <script src="../javascript/jquery-3.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  </head>
  <body>

    <?php include('../html/navbar.html') ?>

    <div class="container">
      <h2>Modifica i tuoi dati</h2>
      <div class="col-sm-6 col-sm-offset-3">
        <div class="panel panel-warning">
          <div class="panel-heading">Dati utente</div>
          <div class="panel-body">
            <?php
            if ($stmt = $mysqli->prepare("SELECT name, surname, gender, email FROM members WHERE username = ?")) {
              $stmt->bind_param('s', $_SESSION["username"]);
              $stmt->execute();
              $stmt->store_result();
              $stmt->bind_result($name, $surname, $gender, $email);
              $stmt->fetch();
              $stmt->close();
            }
            ?>
            <form class="form-horizontal" action="./process_edit_personal_data.php" method="post">
              <div class="form-group">
                <label class="control-label col-sm-3" for="name">Nome:</label>
                <div class="col-sm-9">
                  <input id="name" class="form-control" type="text" name="name" value="<?php echo $name; ?>">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-3" for="surname">Cognome:</label>
                <div class="col-sm-9">
                  <input id="surname" class="form-control" type="text" name="surname" value="<?php echo $surname; ?>">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-3">Genere:</label>
                <div class="col-sm-9">
                  <fieldset>
                    <div class="radio">
                      <label><input type="radio" name="gender" value="m" <?php if ($gender == "m") { echo "checked"; } ?>>Uomo</label>
                    </div>
                    <div class="radio">
                      <label><input type="radio" name="gender" value="f" <?php if ($gender == "f") { echo "checked"; } ?>>Donna</label>
                    </div>
                  </fieldset>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-3" for="email">Email:</label>
                <div class="col-sm-9">
                  <input id="email" class="form-control" type="email" name="email" value="<?php echo $email; ?>">
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                  <button type="button" class="btn btn-warning" onclick="location.href='process_restart_order_list.php'">Indietro</button>
                  <button type="submit" class="btn btn-warning">Modifica</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    <?php include('../html/footer.html') ?>

  </body>
</html>
