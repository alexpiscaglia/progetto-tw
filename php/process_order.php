<?php
include 'db_connection.php';
include 'functions.php';

sec_session_start();

if (login_check($mysqli)) {
  $sql = "INSERT INTO orders (id, username, delivery, address, payment, amount, status) VALUES (NULL, ?, ?, ?, ?, ?, 'Pendente')";
  if ($stmt = $mysqli->prepare($sql)) {
    $stmt->bind_param('ssssd', $_SESSION['username'], $_SESSION['delivery'], $_SESSION['address'],$_SESSION['modality'], $_SESSION['amount']);
    $stmt->execute();
    $stmt->close();
  } else {
    $_SESSION['error'] = "Errore";
    header('Location: ./order_result.php');
  }

  if ($stmt = $mysqli->prepare("SELECT LAST_INSERT_ID()")) {
    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($last_id);
    $stmt->fetch();
    $_SESSION['order_id'] = $last_id;
    $stmt->close();
  } else {
    $_SESSION['error'] = "Error";
    header('Location: ./order_result.php');
  }

  if ($stmt = $mysqli->prepare("INSERT INTO orders_details(order_id, product_id, quantity) VALUES (?, ?, ?)")) {
    foreach ($_SESSION['shoppingcart'] as $product_id => $quantity) {
      $stmt->bind_param('iii', $_SESSION['order_id'], $product_id, $quantity);
      $stmt->execute();
    }
    $stmt->close();

    unset($_SESSION['shoppingcart']);
    unset($_SESSION['delivery']);
    unset($_SESSION['modality']);
    unset($_SESSION['amount']);

    unset($_SESSION['email']);
    unset($_SESSION['pwd']);
    unset($_SESSION['holder']);
    unset($_SESSION['cvv']);
    unset($_SESSION['validity']);
    unset($_SESSION['code']);

    insert_notification("admin", "Nuovo ordine inserito", $mysqli);

    $_SESSION['success'] = "Success";
    header('Location: ./order_result.php');
  } else {
    $_SESSION['error'] = "Error";
    header('Location: ./order_result.php');
  }
} else {
  echo "Richiesta non valida";
  exit();
}

$mysqli->close();
?>
