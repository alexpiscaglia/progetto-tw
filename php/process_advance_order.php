<?php
include 'db_connection.php';
include 'functions.php';

sec_session_start();

if (isset($_POST['order_id'], $_POST['username'], $_POST['status'])) {
  switch ($_POST['status']) {
    case 'Pendente':
      $new_status = "In preparazione";
      break;
    case 'In preparazione':
      $new_status = "In consegna";
      break;
    case 'In consegna':
      $new_status = "Consegnato";
      break;
    default:
      break;
  }
  if ($stmt = $mysqli->prepare("UPDATE orders SET status = ? WHERE id = ? LIMIT 1")) {
    $stmt->bind_param('si', $new_status, $_POST['order_id']);
    $stmt->execute();
    $stmt->close();
    if ($new_status == "In preparazione" || $new_status == "In consegna") {
      insert_notification($_POST['username'], "Il tuo ordine è " . strtolower($new_status), $mysqli);
    }
  }
  header('Location: ./admin-orders_page.php');
}
?>
