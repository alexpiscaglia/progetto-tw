<?php
include 'db_connection.php';
include 'functions.php';

sec_session_start();

if (!login_check($mysqli)) {
  echo "Richiesta non valida";
  exit();
}

if (isset($_POST['modality']) && $_POST['modality'] == "cash") {
  $_SESSION['modality'] = "Contanti";
} else if (isset($_POST['modality']) && $_POST['modality'] == "paypal") {
  $_SESSION['modality'] = "PayPal";
  if ($_POST['email'] != "" && $_POST['pwd'] != "") {
    $_SESSION['email'] = $_POST['email'];
    $_SESSION['pwd'] = $_POST['pwd'];
  } else {
    $_SESSION['error_2'] = "Inserisci tutte le informazioni";
    header("Location: cart_page_2.php");
  }
} else if (isset($_POST['modality']) && $_POST['modality'] == "credit_card") {
  $_SESSION['modality'] = "Carta di credito";
  if ($_POST['holder'] != "" && $_POST['cvv'] != "" && $_POST['validity'] != "" && $_POST['code'] != "") {
    $_SESSION['holder'] = $_POST['holder'];
    $_SESSION['cvv'] = $_POST['cvv'];
    $_SESSION['validity'] = $_POST['validity'];
    $_SESSION['code'] = $_POST['code'];
  } else {
    $_SESSION['error_2'] = "Inserisci tutte le informazioni";
    header("Location: cart_page_2.php");
  }
}
?>
<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Master Piada</title>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Bitter">
    <link rel="stylesheet" type="text/css" href="../css/table.css">
    <link rel="stylesheet" type="text/css" href="../css/general.css">
    <link rel="stylesheet" type="text/css" href="../css/cart.css">
    <link rel="stylesheet" type="text/css" href="../css/navbar.css">
    <link rel="stylesheet" type="text/css" href="../css/footer.css">
    <script src="../javascript/jquery-3.2.1.min.js"></script>
    <script src="../javascript/payment_selection.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  </head>
  <body>

    <?php include('../html/navbar.html') ?>

    <div class="container">
      <div class="progress">
        <div class="progress-bar progress-bar-primary progress-bar-striped active" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
          60%
        </div>
      </div>
    </div>

    <div class="container">
      <?php
      if (isset($_SESSION['error_3'])) {
        create_alert("danger", $_SESSION['error_3']);
        unset($_SESSION['error_3']);
      }
      ?>
      <h1 class="text-center">Indirizzo</h1>
      <div class="col-sm-9">
        <h2>Scegli un indirizzo di consegna</h2>
        <p>Puoi scegliere se farti recapitare l'ordine direttamente in una delle università sottoelencate, all'indirizzo standard da te specificato nel tuo account oppure in un altro luogo</p>
        <?php create_alert("warning", "Attenzione: deve essere entro i confini di Cesena"); ?>

        <form id="address_form" action="cart_summary.php" method="post">
          <fieldset>
            <legend>Indirizzo standard</legend>
            <?php
            if ($stmt = $mysqli->prepare("SELECT street, house_num, cap, city FROM addresses WHERE username = ?")) {
              $stmt->bind_param('s', $_SESSION['username']);
              $stmt->execute();
              $stmt->bind_result($street, $house_num, $cap, $city);
              $stmt->fetch();
            ?>
            <div class="radio">
              <label><input type="radio" name="address" value="default" <?php echo $street == "" ? "disabled" : "checked"; ?>>
                <?php
                if ($street == "") {
                  echo "Indirizzo utente non impostato";
                } else {
                  echo $street . ", " . $house_num . ", " . $cap . " " . $city;
                }
                ?>
              </label>
            </div>
            <?php
              $stmt->close();
            }
            ?>
          </fieldset>

          <fieldset>
            <legend>Università</legend>
            <?php
            if ($stmt = $mysqli->prepare("SELECT * FROM universities")) {
              $stmt->execute();
              $stmt->bind_result($id, $name, $street, $num, $cap, $city, $deleted);
              while ($stmt->fetch()) {
            ?>
            <div class="radio">
              <label><input type="radio" name="address" value="<?php echo $id; ?>"><?php echo $name . " - " . $street . ", " . $num . ", " . $cap . " " . $city; ?></label>
            </div>
            <?php
              }
              $stmt->close();
            }
            ?>
          </fieldset>

          <fieldset>
            <legend>Altro</legend>
            <div class="radio">
              <label><input type="radio" name="address" value="other">Altro</label>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-4" for="other_addr">Indirizzo:</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="other_addr" name="other_addr" placeholder="Via, numero civico">
              </div>
            </div>
          </fieldset>
        </form>
      </div>
      <div class="col-sm-3">
        <div class="panel panel-warning text-center">
          <div class="panel-heading">Scegli un'azione</div>
          <div class="panel-body">
            <p>Attenzione: se scegli la consegna fuori sede verranno aggiunti al totale 2€ per la spedizione</p>
            <button type="button" class="btn btn-warning" onclick="location.href='cart_page_2.php'">Indietro</button>
            <button type="submit" class="btn btn-warning" form="address_form">Avanti</button>
          </div>
        </div>
      </div>
    </div>

    <?php include('../html/footer.html') ?>

  </body>
</html>
