<?php
include 'db_connection.php';
include 'functions.php';

sec_session_start();

if (!admin_login_check($mysqli)) {
  echo "Richiesta non valida";
  exit();
}
?>
<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Master Piada</title>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Bitter">
    <link rel="stylesheet" type="text/css" href="../css/table.css">
    <link rel="stylesheet" type="text/css" href="../css/general.css">
    <link rel="stylesheet" type="text/css" href="../css/navbar.css">
    <script src="../javascript/jquery-3.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="../javascript/admin_universities.js"></script>
  </head>
  <body>

    <?php include('../html/admin_navbar.html'); ?>

    <div class="container">
      <?php check_notifications("admin", $mysqli); ?>
      <h1>Pagina amministratori</h1>
      <?php
      if (isset($_SESSION['success'])) {
        create_alert("success", $_SESSION['success']);
        unset($_SESSION['success']);
      } else if (isset($_SESSION['error'])) {
        create_alert("danger", $_SESSION['error']);
        unset($_SESSION['error']);
      }
      ?>
      <button type="button" class="btn btn-info btn-lg" onclick="reset();" data-toggle="modal" data-target="#myModal">Aggiungi università</button>
      <table class="table table-bordered table-striped">
        <caption>Elenco università servite:</caption>
        <thead>
          <tr>
            <th>Id</th>
            <th>Denominazione</th>
            <th>Via</th>
            <th>N. civico</th>
            <th>CAP</th>
            <th>Città</th>
            <th>Eliminata</th>
            <th>Azione</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if ($stmt = $mysqli->prepare("SELECT * FROM universities")) {
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id, $name, $street, $num, $cap, $city, $deleted);
            while ($stmt->fetch()) {
          ?>
          <tr>
            <td><?php echo $id; ?></td>
            <td><?php echo $name; ?></td>
            <td><?php echo $street; ?></td>
            <td><?php echo $num; ?></td>
            <td><?php echo $cap; ?></td>
            <td><?php echo $city; ?></td>
            <td><?php echo $deleted ? "Si" : "No"; ?></td>
            <td><button type="button" class="btn btn-warning" onclick="loadProduct(<?php echo $id . ",'" . $name . "','" . $street . "'," . $num . ",'" . $cap . "','" . $city . "'," . $deleted; ?>);" data-toggle="modal" data-target="#myModal">Modifica</button></td>
          </tr>
          <?php
            }
            $stmt->close();
          }
          ?>
        </tbody>
      </table>

      <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Dati università</h4>
            </div>
            <div class="modal-body">
              <form action="./process_university.php" method="post">
                <div class="form-group" hidden>
                  <label for="id">Id:</label>
                  <input type="number" class="form-control" id="id" name="id">
                </div>
                <div class="form-group">
                  <label for="name">Denominazione:</label>
                  <input type="text" class="form-control" id="name" placeholder="Denominazione" name="name">
                </div>
                <div class="form-group">
                  <label for="street">Via:</label>
                  <input type="text" class="form-control" id="street" placeholder="Via" name="street">
                </div>
                <div class="form-group">
                  <label for="num">N. civico:</label>
                  <input type="number" class="form-control" id="num" placeholder="N. civico" name="num">
                </div>
                <div class="form-group">
                  <label for="cap">CAP:</label>
                  <input type="text" class="form-control" id="cap" placeholder="CAP" name="cap">
                </div>
                <div class="form-group">
                  <label for="city">Città:</label>
                  <select class="form-control" id="city" name="city">
                    <option value="Cesena">Cesena</option>
                  </select>
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" id="deleted" name="deleted" value="true"> Eliminata</label>
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

  </body>
</html>
