<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Master Piada</title>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Bitter">
    <link rel="stylesheet" type="text/css" href="../css/general.css">
    <link rel="stylesheet" type="text/css" href="../css/table.css">
    <link rel="stylesheet" type="text/css" href="../css/navbar.css">
    <link rel="stylesheet" type="text/css" href="../css/footer.css">
    <script src="../javascript/jquery-3.2.1.min.js"></script>
    <script src="../javascript/table.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  </head>
  <body>

    <?php
    include 'db_connection.php';
    include 'functions.php';

    sec_session_start();

    include('navbar.php');
    ?>

    <div class="container">
      <?php
      if (login_check($mysqli)) {
        check_notifications($_SESSION['username'], $mysqli);
      }

      if (isset($_SESSION['success'])) {
        create_alert("success", $_SESSION['success']);
        unset($_SESSION['success']);
      } else if (isset($_SESSION['error'])) {
        create_alert("danger", $_SESSION['error']);
        unset($_SESSION['error']);
      }

      switch ($_GET["type"]) {
        case 'Crescione':
          $title = "Crescioni";
          break;
        case 'Panino':
          $title = "Panini";
          break;
        case 'Bevanda':
          $title = "Bevande";
          break;
        default:
          $title = "Piadine";
          break;
      }
      ?>
      <div class="text-center">
        <h1><?php echo $title; ?></h1>
      </div>
      <div class="col-sm-9">
        <label for="myInput">Cerca</label>
        <input class="form-control" id="myInput" type="text" placeholder="Cerca.."><br/>
        <table class="table table-bordered table-striped text-center">
          <thead>
            <tr>
              <th>Nome</th>
              <th>Ingredienti</th>
              <th>Prezzo (€)</th>
              <th>Quantità</th>
              <th>Azione</th>
            </tr>
          </thead>
          <tbody id="myTable">
            <?php
            if ($stmt = $mysqli->prepare("SELECT id, name, ingredients, price FROM products WHERE type = ? AND deleted = 0")) {
              $stmt->bind_param('s', $_GET["type"]);
              $stmt->execute();
              $stmt->store_result();
              $stmt->bind_result($product_id, $name, $ingredients, $price);
              while ($stmt->fetch()) {
            ?>
            <tr>
              <td><?php echo $name ?></td>
              <td><?php echo $ingredients ?></td>
              <td><?php echo sprintf("%01.2f", $price); ?></td>
              <form action="process_add_to_cart.php" method="post">
                <td>
                  <input id="product_id" type="hidden" name="product_id" value="<?php echo $product_id ?>">
                  <label for="quantity" hidden>Quantità</label>
                  <input id="quantity" type="number" name="quantity" class="form-control" value="1" min="1" max="10" style="width: 60px">
                </td>
                <td>
                  <input type="submit" class="btn btn-success" value="Aggiungi">
                </td>
              </form>
            </tr>
            <?php
              }
              $stmt->close();
            }
  					?>
          </tbody>
        </table>
      </div>

      <div class="col-sm-3">
        <h4>Hai <?php echo isset($_SESSION['shoppingcart']) ? count($_SESSION['shoppingcart']) : 0; ?> prodotto/i nel carrello</h4>
        <div class="panel panel-warning">
          <div class="panel-heading">Attenzione</div>
          <div class="panel-body text-center">
            <p>È consentito ordinare al massimo 10 unità dello stesso prodotto</p>
            <?php
            if (login_check($mysqli)) {
            ?>
            <button type="button" class="btn btn-warning btn-lg" onclick="location.href='cart_page_1.php'">Vai al carrello</button>
            <?php
            } else {
              create_alert("danger", "Effettua il login per accedere al carrello");
            }
            ?>
          </div>
        </div>
      </div>
    </div>

    <?php include('../html/footer.html'); ?>

  </body>
</html>
