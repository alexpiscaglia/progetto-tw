<?php
include 'db_connection.php';
include 'functions.php';

sec_session_start();

if (!login_check($mysqli)) {
  echo "Richiesta non valida";
  exit();
}

if (empty($_SESSION['shoppingcart'])) {
  $_SESSION['error_1'] = "Aggiungi qualcosa al carrello per procedere";
  header("Location: cart_page_1.php");
}

if (isset($_POST['delivery'])) {
  $delivery = strtotime($_POST['delivery']);
  $now = strtotime(date('H:i'));
  if ((($delivery - $now) / 60) >= 2 * 60) {
    $_SESSION['delivery'] = $_POST['delivery'];
  } else {
    $_SESSION['error_1'] = "È necessario ordinare con almeno 2 ore di anticipo";
    header("Location: cart_page_1.php");
  }
}
?>
<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Master Piada</title>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Bitter">
    <link rel="stylesheet" type="text/css" href="../css/table.css">
    <link rel="stylesheet" type="text/css" href="../css/general.css">
    <link rel="stylesheet" type="text/css" href="../css/cart.css">
    <link rel="stylesheet" type="text/css" href="../css/navbar.css">
    <link rel="stylesheet" type="text/css" href="../css/footer.css">
    <script src="../javascript/jquery-3.2.1.min.js"></script>
    <script src="../javascript/payment_selection.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  </head>
  <body>

    <?php include('../html/navbar.html') ?>

    <div class="container">
      <div class="progress">
        <div class="progress-bar progress-bar-primary progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
          40%
        </div>
      </div>
    </div>

    <div class="container">
      <?php
      if (isset($_SESSION['error_2'])) {
        create_alert("danger", $_SESSION['error_2']);
        unset($_SESSION['error_2']);
      }
      ?>
      <h1 class="text-center">Pagamento</h1>
      <div class="form-group">
        <label class="control-label col-sm-4 text-right" for="modality">Modalità:</label>
        <div class="col-sm-4">
          <select id="modality" class="form-control" onchange="display(this);">
            <option>Contanti</option>
            <option>PayPal</option>
            <option>Carta di credito</option>
          </select>
        </div>
      </div>
    </div>

    <div class="container modality">
      <div class="col-sm-9">
        <img src="../img/cash_icon.png" class="img-responsive col-sm-4" alt="Icona contanti">
      </div>
      <div class="col-sm-3">
        <div class="panel panel-warning">
          <div class="panel-heading">Scegli un'azione</div>
          <div class="panel-body text-center">
            <form action="cart_page_3.php" method="post">
              <input type="hidden"  name="modality" value="cash">
              <button type="button" class="btn btn-warning" onclick="location.href='cart_page_1.php'">Indietro</button>
              <button type="submit" class="btn btn-warning">Avanti</button>
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class="container modality">
      <div class="col-sm-9">
        <img src="../img/paypal_icon.jpg" class="img-responsive col-sm-4" alt="PayPal">
        <form class="form-horizontal col-sm-8" id="paypal_form" action="cart_page_3.php?modality=paypal" method="post">
          <input type="hidden" name="modality" value="paypal">
          <div class="form-group">
            <label class="control-label col-sm-4" for="email">Email:</label>
            <div class="col-sm-8">
              <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-4" for="pwd">Password:</label>
            <div class="col-sm-8">
              <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pwd">
            </div>
          </div>
        </form>
      </div>
      <div class="col-sm-3">
        <div class="panel panel-warning">
          <div class="panel-heading">Scegli un'azione</div>
          <div class="panel-body text-center">
            <button type="button" class="btn btn-warning" onclick="location.href='cart_page_1.php'">Indietro</button>
            <button type="submit" class="btn btn-warning" form="paypal_form">Avanti</button>
          </div>
        </div>
      </div>
    </div>

    <div class="container modality">
      <div class="col-sm-9">
        <img src="../img/credit_card_icon.png" class="img-responsive col-sm-4" alt="Carta di credito">
        <form class="form-horizontal col-sm-8" id="credit_card_form" action="cart_page_3.php" method="post">
          <input type="hidden" name="modality" value="credit_card">
          <div class="form-group">
            <label class="control-label col-sm-4" for="holder">Nome intestatario:</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="holder" placeholder="Nome" name="holder">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-4" for="cvv">CVV:</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="cvv" placeholder="CVV" name="cvv">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-4" for="validity">Validità:</label>
            <div class="col-sm-8">
              <input type="date" class="form-control" id="validity" name="validity">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-4" for="code">Codice carta:</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="code" placeholder="Codice" name="code">
            </div>
          </div>
        </form>
      </div>
      <div class="col-sm-3">
        <div class="panel panel-warning">
          <div class="panel-heading">Scegli un'azione</div>
          <div class="panel-body text-center">
            <button type="button" class="btn btn-warning" onclick="location.href='cart_page_1.php'">Indietro</button>
            <button type="submit" class="btn btn-warning" form="credit_card_form">Avanti</button>
          </div>
        </div>
      </div>
    </div>

    <?php include('../html/footer.html') ?>

  </body>
</html>
