<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="./home.php">Master Piada</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="./home.php">Home</a></li>
        <li><a href="./about_us.php">Chi siamo</a></li>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Menù
          <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="./table.php?type=Piadina">Piadine</a></li>
            <li><a href="./table.php?type=Crescione">Crescioni</a></li>
            <li><a href="./table.php?type=Panino">Panini</a></li>
            <li><a href="./table.php?type=Bevanda">Bevande</a></li>
          </ul>
        </li>
        <li><a href="./process_restart_review.php">Recensioni</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <?php
        if (login_check($mysqli)) {
        ?>
        <li><a href="./process_restart_order_list.php"><span class="glyphicon glyphicon-user"></span> Account</a></li>
        <li><a href="./cart_page_1.php"><span class="glyphicon glyphicon-shopping-cart"></span> Carrello</a></li>
        <li><a href="./process_logout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
        <?php
        } else {
        ?>
        <li><a href="./subscription.php"><span class="glyphicon glyphicon-user"></span> Iscriviti</a></li>
        <li><a href="./login.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
        <?php
        }
        ?>
      </ul>
    </div>
  </div>
</nav>
