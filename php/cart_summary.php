<?php
include 'db_connection.php';
include 'functions.php';

sec_session_start();

if (!login_check($mysqli)) {
  echo "Richiesta non valida";
  exit();
}

if (isset($_POST['address'])) {
  switch ($_POST['address']) {
    case 'default':
      if ($stmt = $mysqli->prepare("SELECT street, house_num, cap, city, info FROM addresses WHERE username = ?")) {
        $stmt->bind_param('s', $_SESSION['username']);
        $stmt->execute();
        $stmt->bind_result($street, $house_num, $cap, $city, $info);
        $stmt->fetch();
        $_SESSION['address'] = $street . ", " . $house_num . ", " . $cap . " " . $city . " - " . $info;
        $stmt->close();
      }
      break;
    case 'other':
      if (isset($_POST['other_addr']) && $_POST['other_addr'] != "") {
        $_SESSION['address'] = $_POST['other_addr'] . ", 47521 Cesena";
      } else {
        $_SESSION['error_3'] = "Digitare l'indirizzo";
        header("Location: cart_page_3.php");
      }
      break;
    default:
      if ($stmt = $mysqli->prepare("SELECT * FROM universities WHERE id = ?")) {
        debug_to_console($_POST['address']);
        $stmt->bind_param('i', $_POST['address']);
        $stmt->execute();
        $stmt->bind_result($id, $name, $street, $num, $cap, $city, $deleted);
        $stmt->fetch();
        $_SESSION['address'] = $street . ", " . $num . ", " . $cap . " " . $city;
        $stmt->close();
      }
      break;
  }
} else {
  $_SESSION['error_3'] = "Selezionare un indirizzo";
  header("Location: cart_page_3.php");
}

?>

<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Master Piada</title>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Bitter">
    <link rel="stylesheet" type="text/css" href="../css/table.css">
    <link rel="stylesheet" type="text/css" href="../css/general.css">
    <link rel="stylesheet" type="text/css" href="../css/cart.css">
    <link rel="stylesheet" type="text/css" href="../css/navbar.css">
    <link rel="stylesheet" type="text/css" href="../css/footer.css">
    <script src="../javascript/jquery-3.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  </head>
  <body>

    <?php include('../html/navbar.html') ?>

    <div class="container">
      <div class="progress">
        <div class="progress-bar progress-bar-primary progress-bar-striped active" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
          80%
        </div>
      </div>
    </div>

    <div class="container text-center">
      <h1>Riepilogo</h1>
      <div class="col-sm-9">
        <table class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>Tipo</th>
              <th>Nome</th>
              <th>Prezzo (€)</th>
              <th>Quantità</th>
            </tr>
          </thead>
          <tbody id="myTable">
            <?php
            $amount = 0;

            if (isset($_SESSION['shoppingcart'])) {
              $sql = "SELECT * FROM products WHERE id IN (";

              foreach ($_SESSION['shoppingcart'] as $product_id => $quantity) {
                $sql .= $product_id . ",";
              }

              $sql = substr($sql, 0, -1) . ") ORDER BY name ASC";

              if ($stmt = $mysqli->prepare($sql)) {
                $stmt->execute();
                $stmt->store_result();
                $stmt->bind_result($product_id, $type, $name, $ingredients, $price, $deleted);
                while ($stmt->fetch()) {
            ?>
            <tr>
              <td><?php echo $type; ?></td>
              <td><?php echo $name; ?></td>
              <td><?php echo sprintf("%01.2f", $price); ?></td>
              <td><?php echo $_SESSION['shoppingcart'][$product_id]; ?></td>
            </tr>
            <?php
                  $amount += $price * $_SESSION['shoppingcart'][$product_id];
                }
                $stmt->close();
              }
              $mysqli->close();
            }

            if ($_POST['address'] == "default" || $_POST['address'] == "other") {
              $amount += 2;
            }
            $_SESSION['amount'] = $amount;
  					?>
          </tbody>
        </table>
        <div class="panel panel-info">
          <div class="panel-heading">Dettagli:</div>
          <div class="panel-body">
            <p><strong>Orario:</strong> <?php echo $_SESSION['delivery']; ?></p>
            <p><strong>Luogo:</strong> <?php echo $_SESSION['address']; ?></p>
            <p><strong>Pagamento:</strong> <?php echo $_SESSION['modality']; ?></p>
            <?php
            if ($_SESSION['modality'] == "PayPal") {
              echo "<p><strong>Account:</strong> " . $_SESSION['email'] . "</p>";
            } else if ($_SESSION['modality'] == "Carta di credito") {
              echo "<p><strong>Intestatario:</strong> " . $_SESSION['holder'] . "</p>";
              echo "<p><strong>Codice carta:</strong> termina con " . substr($_SESSION['code'], -4) . "</p>";
            }
            ?>
          </div>
        </div>
      </div>

      <div class="col-sm-3">
        <div class="panel panel-warning">
          <div class="panel-heading">Scegli un'azione</div>
          <div class="panel-body">
            <p>Totale: <?php echo sprintf("%01.2f", $amount); ?> €</p>
            <button type="button" class="btn btn-warning" onclick="location.href='cart_page_3.php'">Indietro</button>
            <button type="submit" class="btn btn-warning" onclick="location.href='process_order.php'">Conferma</button>
          </div>
        </div>
      </div>
    </div>

    <?php include('../html/footer.html') ?>

  </body>
</html>
