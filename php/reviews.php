<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Master Piada</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Bitter">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/review.css">
    <link rel="stylesheet" href="../css/general.css">
    <link rel="stylesheet" href="../css/blogcomment.css">
    <link rel="stylesheet" href="../css/navbar.css">
    <link rel="stylesheet" href="../css/footer.css">
    <script src="../javascript/jquery-3.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  </head>
  <body>

    <?php
    include 'db_connection.php';
    include 'functions.php';

    sec_session_start();

    include('navbar.php');
    ?>

    <div class="container bootstrap snippet">
      <?php
      if (login_check($mysqli)) {
        check_notifications($_SESSION['username'], $mysqli);
      }

      if (isset($_SESSION['success'])) {
        echo "<br/>";
        create_alert("success", $_SESSION['success']);
        unset($_SESSION['success']);
      } else if (isset($_SESSION['error'])) {
        echo "<br/>";
        create_alert("danger", $_SESSION['error']);
        unset($_SESSION['error']);
      }
      ?>
      <div class="row">
        <div class="col-sm-9">
          <div class="blog-comment">
				    <h1>Recensioni</h1>
            <hr/>
				    <ul class="comments">
              <?php
              $offset = $_SESSION['rev_page_num'] * $_SESSION['rev_scale'];
              if ($stmt = $mysqli->prepare("SELECT * FROM reviews ORDER BY date desc LIMIT ?,?")) {
                $stmt->bind_param('ii', $offset, $_SESSION['rev_scale']);
                $stmt->execute();
                $stmt->store_result();
                $stmt->bind_result($review_id, $username, $date, $review, $rating);
                while ($stmt->fetch()) {
              ?>
              <li class="clearfix">
                <?php
                  if (is_male($username, $mysqli)) {
                ?>
                <img src="../img/male_user_icon.jpg" class="avatar" alt="">
                <?php
                  } else if (is_female($username, $mysqli)) {
                ?>
                <img src="../img/female_user_icon.jpg" class="avatar" alt="">
                <?php
                  }
                ?>
                <div class="post-comments">
                  <p class="meta">
                    Il <strong class="review-desc"><?php echo format_date($date) . " " . $username; ?></strong> ha detto:
                    <span class="star-container">
                      <?php
                      for ($i = 0; $i < $rating; $i++) {
                        echo "<span class='fa fa-star checked'></span>";
                      }
                      for ($i = 0; $i < 5 - $rating; $i++) {
                        echo "<span class='fa fa-star'></span>";
                      }
                      ?>
                    </span>
                  </p>
                  <p><?php echo $review ?></p>
                </div>
              </li>
              <?php
                }
                $stmt->close();
    					}
    					?>
				    </ul>
			    </div>
          <ul class="pager">
            <li><a href="process_prev_review.php">Previous</a></li>
            <li><a href="process_next_review.php">Next</a></li>
          </ul>
        </div>
        <div class="col-sm-3">
          <div class="panel panel-info">
            <div class="panel-heading">
              <label for="comment">Scrivi una breve recensione</label>
            </div>
            <div class="panel-body text-center">
              <form action="process_add_review.php" method="post">
                <textarea class="form-control" name="comment" rows="10" maxlength="250" placeholder="Max 250 caratteri..." id="comment"></textarea>
                <div class="vertical-center horizontal-center">
                  <label for="rating">Valutazione</label>
                  <input id="rating" type="number" name="rating" class="form-control" value="1" min="1" max="5">
                </div>
                <br/>
                <?php
                if (login_check($mysqli)) {
                ?>
                <button type="submit" class="btn btn-primary">Conferma</button>
                <?php
                } else {
                  create_alert("danger", "Effettua il login per lasciare una recensione");
                }
                ?>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

    <?php include('../html/footer.html') ?>

  </body>
</html>
