<?php
include 'db_connection.php';
include 'functions.php';

sec_session_start();

if (isset($_POST['id'], $_POST['type'], $_POST['name'], $_POST['ingredients'], $_POST['price'])) {
  $deleted = isset($_POST['deleted']) ? 1 : 0;
  if ($_POST['id'] == "" && $stmt = $mysqli->prepare("INSERT INTO products(id, type, name, ingredients, price, deleted) VALUES (null, ?, ?, ?, ?, ?)")) {
    $stmt->bind_param('sssdi', $_POST['type'], $_POST['name'], $_POST['ingredients'], $_POST['price'], $deleted);
    $stmt->execute();
    $stmt->close();
    $_SESSION['success'] = "Prodotto inserito con successo";
  } else if ($_POST['id'] != "" && $stmt = $mysqli->prepare("UPDATE products SET type = ?, name = ?, ingredients = ?, price = ?, deleted = ? WHERE id = ?")) {
    $stmt->bind_param('sssdii', $_POST['type'], $_POST['name'], $_POST['ingredients'], $_POST['price'], $deleted, $_POST['id']);
    $stmt->execute();
    $stmt->close();
    $_SESSION['success'] = "Prodotto modificto con successo";
  } else {
    $_SESSION['error'] = "Errore del database";
    header('Location: ./admin-products_page.php');
  }
  header('Location: ./admin-products_page.php');
} else {
  echo "Richiesta non valida";
}
?>
