<?php
include 'db_connection.php';
include 'functions.php';

sec_session_start();

if (!admin_login_check($mysqli)) {
  echo "Richiesta non valida";
  exit();
}
?>
<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Master Piada</title>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Bitter">
    <link rel="stylesheet" type="text/css" href="../css/table.css">
    <link rel="stylesheet" type="text/css" href="../css/general.css">
    <link rel="stylesheet" type="text/css" href="../css/navbar.css">
    <script src="../javascript/jquery-3.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="../javascript/admin_products.js"></script>
  </head>
  <body>

    <?php include('../html/admin_navbar.html'); ?>

    <div class="container">
      <?php check_notifications("admin", $mysqli); ?>
      <h1>Pagina amministratori</h1>
      <?php
      if (isset($_SESSION['success'])) {
        create_alert("success", $_SESSION['success']);
        unset($_SESSION['success']);
      } else if (isset($_SESSION['error'])) {
        create_alert("danger", $_SESSION['error']);
        unset($_SESSION['error']);
      }
      ?>
      <button type="button" class="btn btn-info btn-lg" onclick="reset();" data-toggle="modal" data-target="#myModal">Aggiungi prodotto</button>
      <table class="table table-bordered table-striped">
        <caption>Elenco prodotti:</caption>
        <thead>
          <tr>
            <th>Id</th>
            <th>Tipo</th>
            <th>Nome</th>
            <th>Ingredienti</th>
            <th>Prezzo (€)</th>
            <th>Fuori produzione</th>
            <th>Azione</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if ($stmt = $mysqli->prepare("SELECT id, type, name, ingredients, price, deleted FROM products")) {
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id, $type, $name, $ingredients, $price, $deleted);
            while ($stmt->fetch()) {
          ?>
          <tr>
            <td><?php echo $id; ?></td>
            <td><?php echo $type; ?></td>
            <td><?php echo $name; ?></td>
            <td><?php echo $ingredients; ?></td>
            <td><?php echo sprintf("%01.2f", $price); ?></td>
            <td><?php echo $deleted ? "Si" : "No"; ?></td>
            <td><button type="button" class="btn btn-warning" onclick="loadProduct(<?php echo $id . ",'" . $type . "','" . $name . "','" . $ingredients . "'," . $price . "," . $deleted; ?>);" data-toggle="modal" data-target="#myModal">Modifica</button></td>
          </tr>
          <?php
            }
            $stmt->close();
          }
          ?>
        </tbody>
      </table>

      <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Dati prodotto</h4>
            </div>
            <div class="modal-body">
              <form action="./process_product.php" method="post">
                <div class="form-group" hidden>
                  <label for="id">Id:</label>
                  <input type="number" class="form-control" id="id" name="id">
                </div>
                <div class="form-group">
                  <label for="type">Tipo:</label>
                  <select class="form-control" id="type" name="type">
                    <option value="Piadina">Piadina</option>
                    <option value="Crescione">Crescione</option>
                    <option value="Panino">Panino</option>
                    <option value="Bevanda">Bevanda</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="name">Nome:</label>
                  <input type="text" class="form-control" id="name" placeholder="Nome" name="name">
                </div>
                <div class="form-group">
                  <label for="ingredients">Ingredienti:</label>
                  <input type="text" class="form-control" id="ingredients" placeholder="Ingredienti" name="ingredients">
                </div>
                <div class="form-group">
                  <label for="price">Prezzo (€):</label>
                  <input type="number" class="form-control" id="price" placeholder="Prezzo" name="price" min="0" step="0.01">
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" id="deleted" name="deleted" value="true"> Fuori produzione</label>
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

  </body>
</html>
