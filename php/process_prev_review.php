<?php

include 'functions.php';

sec_session_start();

if ($_SESSION['rev_page_num'] != 0) {
  $_SESSION['rev_page_num'] = $_SESSION['rev_page_num'] - 1;
}

header('Location: reviews.php');

?>
