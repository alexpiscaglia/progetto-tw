<?php
include 'db_connection.php';
include 'functions.php';

sec_session_start();

if (!login_check($mysqli)) {
  echo "Richiesta non valida";
  exit();
}

$empty = true;
?>
<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Master Piada</title>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Bitter">
    <link rel="stylesheet" type="text/css" href="../css/table.css">
    <link rel="stylesheet" type="text/css" href="../css/general.css">
    <link rel="stylesheet" type="text/css" href="../css/cart.css">
    <link rel="stylesheet" type="text/css" href="../css/navbar.css">
    <link rel="stylesheet" type="text/css" href="../css/footer.css">
    <script src="../javascript/jquery-3.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  </head>
  <body>

    <?php include('../html/navbar.html') ?>

    <div class="container">
      <div class="progress">
        <div class="progress-bar progress-bar-primary progress-bar-striped active" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
          20%
        </div>
      </div>
    </div>

    <div class="container text-center">
      <?php
      check_notifications($_SESSION['username'], $mysqli);

      if (isset($_SESSION['error_1'])) {
        create_alert("danger", $_SESSION['error_1']);
        unset($_SESSION['error_1']);
      }
      ?>
      <h1>Il tuo ordine</h1>
      <div class="col-sm-9">
        <table class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>Tipo</th>
              <th>Nome</th>
              <th>Prezzo (€)</th>
              <th>Quantità</th>
              <th>Azione</th>
            </tr>
          </thead>
          <tbody id="myTable">
            <?php
            $total_price = 0;

            if (isset($_SESSION['shoppingcart'])) {
              $sql = "SELECT * FROM products WHERE id IN (";

              foreach ($_SESSION['shoppingcart'] as $product_id => $quantity) {
                $sql .= $product_id . ",";
              }

              $sql = substr($sql, 0, -1) . ") ORDER BY name ASC";

              if ($stmt = $mysqli->prepare($sql)) {
                $stmt->execute();
                $stmt->store_result();
                $stmt->bind_result($product_id, $type, $name, $ingredients, $price, $deleted);
                $empty = false;
                while ($stmt->fetch()) {
            ?>
            <tr>
              <td><?php echo $type; ?></td>
              <td><?php echo $name; ?></td>
              <td><?php echo sprintf("%01.2f", $price); ?></td>
              <td><?php echo $_SESSION['shoppingcart'][$product_id]; ?></td>
              <td>
                <form action="process_remove_from_cart.php" method="post">
                  <label for="product_id" hidden>ID prodotto</label>
                  <input id="product_id" type="number" name="product_id" value="<?php echo $product_id ?>" hidden>
                  <input class="btn btn-danger" type="submit" value="Rimuovi">
                </form>
              </td>
            </tr>
            <?php
                  $total_price += $price * $_SESSION['shoppingcart'][$product_id];
                }
                $stmt->close();
              }
              $mysqli->close();
            }
  					?>
          </tbody>
        </table>
        <?php
        if ($empty) {
          echo "<p>Il carrello è vuoto</p>";
        }
        ?>
      </div>

      <div class="col-sm-3">
        <h4>Totale provvisorio: <?php echo sprintf("%01.2f", $total_price); ?> €</h4>
        <div class="panel panel-warning">
          <div class="panel-heading">Attenzione</div>
          <div class="panel-body">Si ricorda che per un adeguata organizzazione è necessario ordinare con almeno 2 ore di anticipo
            <form id="time_select_form" action="./cart_page_2.php" method="post">
              <label for="delivery">Orario di consegna (hh:mm)</label>
              <input id="delivery" type="time" class="form-control" name="delivery" value="09:00" min="09:00" max="17:00">
            </form>
            <button type="button" class="btn btn-warning" onclick="location.href='table.php?type=Piadina'">Indietro</button>
            <button type="submit" class="btn btn-warning" form="time_select_form">Avanti</button>
          </div>
        </div>
      </div>
    </div>

    <?php include('../html/footer.html') ?>

  </body>
</html>
