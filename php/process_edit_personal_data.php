<?php
include 'db_connection.php';
include 'functions.php';

sec_session_start();

if (isset($_POST['name'], $_POST['surname'], $_POST['gender'], $_POST['email'])) {
  if ($stmt = $mysqli->prepare("UPDATE members SET name = ?, surname = ?, gender = ?, email = ? WHERE username = ?")) {
    $stmt->bind_param('sssss', $_POST['name'], $_POST['surname'], $_POST['gender'], $_POST['email'], $_SESSION["username"]);
    $stmt->execute();
    if ($stmt->affected_rows == 0) {
      $_SESSION['error'] = "È stato riscontrato un errore, riprova perfavore";
    } else {
      $_SESSION['success'] = "Modifica effettuata con successo";
    }
    $stmt->close();
    header('Location: ./process_restart_order_list.php');
  } else {
    $_SESSION['error'] = "Errore del database";
    header('Location: ./process_restart_order_list.php');
  }
} else {
  echo "Richiesta non valida";
}
?>
