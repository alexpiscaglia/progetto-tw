<?php

include 'functions.php';

sec_session_start();

if (($_SESSION['ord_page_num'] + 1) * $_SESSION['ord_scale'] < $_SESSION['ord_count']) {
  $_SESSION['ord_page_num'] = $_SESSION['ord_page_num'] + 1;
}

header('Location: account.php');

?>
