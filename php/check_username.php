<?php
include 'db_connection.php';
include 'functions.php';

sec_session_start();

$username = $_POST['username'];
if ($stmt = $mysqli->prepare("SELECT * FROM members WHERE username = ?")) {
  $stmt->bind_param('s', $username);
  $stmt->execute();
  $stmt->store_result();
  if ($stmt->num_rows > 0) {
    echo "taken";
  } else {
    echo 'not_taken';
  }
  $stmt->close();
  exit();
}
?>
