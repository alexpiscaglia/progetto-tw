<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Registrazione</title>
    <link rel="stylesheet" type="text/css" href="../css/form.css">
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Bitter">
    <script src="../javascript/jquery-3.2.1.min.js"></script>
    <script src="../javascript/sha512.js"></script>
    <script src="../javascript/forms.js"></script>
    <script src="../javascript/check_sub.js"></script>
  </head>
  <body>
    <h1>Master Piada</h1>
    <section>
      <header>
        <h2>Iscriviti subito!</h2>
        <h3>iscriviti e accedi al nostro sito</h3>
      </header>
      <?php
      include 'functions.php';

      sec_session_start();

      if (isset($_SESSION['error'])) {
        echo "<p class='error-msg'>" . $_SESSION['error'] . "</p>";
        unset($_SESSION['error']);
      }
      ?>
      <form id="sub_form" action="process_subscription.php" method="post">
        <fieldset>
          <legend><span>1</span> Generalit&agrave;</legend>
          <ul>
            <li><label for="name">Nome</label></li>
            <li><input id="name" type="text" name="name" autocomplete="on" required></li>
            <li><label for="surname">Cognome</label></li>
            <li><input id="surname" type="text" name="surname" autocomplete="on" required></li>
            <li><label>Genere</label></li>
            <li><input id="male" type="radio" name="gender" value="m" checked><label for="male"> Uomo</label></li>
            <li><input id="female" type="radio" name="gender" value="f"><label for="female"> Donna</label></li>
            <li><label for="email">E-mail</label></li>
            <li><input id="email" type="email" name="email" autocomplete="on" required></li>
          </ul>
        </fieldset>
        <fieldset>
          <legend><span>2</span> Username &amp; Password</legend>
          <ul>
            <li><label for="username">Username</label></li>
            <li><input id="username" type="text" name="username" autocomplete="on" required></li>
            <li><label for="password">Password</label></li>
            <li><input id="password" type="password" name="password" required></li>
            <li><label for="confirm_pwd">Conferma password</label></li>
            <li><input id="confirm_pwd" type="password" name="confirm_pwd" required></li>
          </ul>
        </fieldset>
        <div>
          <label for="privacy"><input id="privacy" type="checkbox" name="privacy" required> Accetto <a href="#">termini e condizioni</a>.</label>
          <input type="button" value="Invia" onclick="check_sub(this.form, this.form.password)">
          <input type="reset" value="Reset">
        </div>
      </form>
      <footer>Hai già un account? <a href="./login.php">Accedi</a>.</footer>
    </section>
  </body>
</html>
