var name_status = false;
var surname_status = false;
var email_status = false;
var username_status = false;
var password_status = false;
var confirm_pwd_status = false;

$(document).ready(function() {

  $("#name").on("focusout", function() {
		var name = $("#name").val();

    $(this).next("span").remove();
		if (name.length == 0) {
			$(this).css("border-color", "red");
      $(this).after("<span>Inserire il nome</span>");
      name_status = false;
		} else {
      $(this).removeAttr('style');
      $(this).next("span").remove();
      name_status = true;
		}
	});

  $("#surname").on("focusout", function() {
		var surname = $("#surname").val();

    $(this).next("span").remove();
		if (surname.length == 0) {
			$(this).css("border-color", "red");
      $(this).after("<span>Inserire il cognome</span>");
      surname_status = false;
		} else {
      $(this).removeAttr('style');
      $(this).next("span").remove();
      surname_status = true;
		}
	});

  $("#email").on("focusout", function() {
		var value = $("#email").val();
		var res = value.match(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/);

    $(this).next("span").remove();
		if (res == null) {
			$(this).css("border-color", "red");
      $(this).after("<span>Inserire un indirizzo e-mail valido</span>");
      email_status = false;
		} else {
      $(this).removeAttr('style');
      $(this).next("span").remove();
      email_status = true;
		}
	});

  $("#username").on("focusout", function() {
		var username = $("#username").val();
		var res = username.match(/^[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/);

    $(this).next("span").remove();
		if (username.length < 4 || res == null) {
			$(this).css("border-color", "red");
      $(this).after("<span>Lo username deve contenere almeno 4 caratteri alfanumerici</span>");
      username_status = false;
    } else {
      $.ajax({
        url: '../php/check_username.php',
        method: 'post',
        data: {
        	'username' : username,
        },
        success: function(result) {
          if (result == 'taken') {
            $("#username").css("border-color", "red");
            $("#username").after("<span>Username già in uso</span>");
            username_status = false;
          } else if (result == 'not_taken') {
            $("#username").removeAttr('style');
            $("#username").next("span").remove();
            username_status = true;
          }
        }
      });
    }
	});

  $("#password").on("focusout", function() {
		var password = $("#password").val();
		var res = password.match(/^[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/);

    $(this).next("span").remove();
		if (password.length < 8 || res == null) {
			$(this).css("border-color", "red");
      $(this).after("<span>La password deve contenere almeno 8 caratteri alfanumerici</span>");
      password_status = false;
    } else {
      $(this).removeAttr('style');
      $(this).next("span").remove();
      password_status = true;
    }
	});

  $("#confirm_pwd").on("focusout", function() {
    var password = $("#password").val();
		var confirm_pwd = $("#confirm_pwd").val();

    $(this).next("span").remove();
		if (password !== confirm_pwd) {
			$(this).css("border-color", "red");
      $(this).after("<span>Le password non coincidono</span>");
      confirm_pwd_status = false;
    } else {
      $(this).removeAttr('style');
      $(this).next("span").remove();
      confirm_pwd_status = true;
    }
	});

});

function check_sub(form, password) {
  if (name_status == true && surname_status == true && email_status == true
      && username_status == true && password_status == true && confirm_pwd_status == true) {
    formhash(form, password);
  } else {
    $("p.error-msg").remove();
    $("div").before("<p class='error-msg'>Inserisci correttamente tutte le informazioni</p>");
  }
}
