var modalities;
var visible = 0;

$(document).ready(function() {
  modalities = document.getElementsByClassName("modality");
  var size = modalities.length;

  for (var i = 1; i < size; i++) {
    modalities[i].classList.add("hidden");
  }
});

function display(elem) {
  modalities[visible].classList.add("hidden");
  visible = elem.selectedIndex;
  modalities[visible].classList.remove("hidden");
}
