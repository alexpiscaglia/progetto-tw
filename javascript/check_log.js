function check_log(form, password) {
  var username = $("#username").val();
  var pwd = $("#password").val();

  if (username.length == 0 || pwd.length == 0) {
    $("p.error-msg").remove();
    $("div").before("<p class='error-msg'>Inserisci correttamente tutte le informazioni</p>");
  } else {
    formhash(form, password);
  }
}
