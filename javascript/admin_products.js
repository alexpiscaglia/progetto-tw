function loadProduct(id, type, name, ingredients, price, deleted) {
  $("#id").val(id);
  $("#type").val(type);
  $("#name").val(name);
  $("#ingredients").val(ingredients);
  $("#price").val(price);
  $("#deleted").prop("checked", deleted);
}

function reset() {
  $("#id").val('');
  $("#type").val('Piadina');
  $("#name").val('');
  $("#ingredients").val('');
  $("#price").val('');
  $("#deleted").prop("checked", false);
}
