function myMap() {
  var mapCanvas = document.getElementById("map");
  var mapOptions = { center: new google.maps.LatLng(44.138819, 12.2416815), zoom: 10 };
  var map = new google.maps.Map(mapCanvas, mapOptions);
}
