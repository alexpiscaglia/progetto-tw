function loadProduct(id, name, street, num, cap, city, deleted) {
  $("#id").val(id);
  $("#name").val(name);
  $("#street").val(street);
  $("#num").val(num);
  $("#cap").val(cap);
  $("#city").val(city);
  $("#deleted").prop("checked", deleted);
}

function reset() {
  $("#id").val('');
  $("#name").val('');
  $("#street").val('');
  $("#num").val('');
  $("#cap").val('');
  $("#city").val('Cesena');
  $("#deleted").prop("checked", false);
}
