-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Lug 01, 2018 alle 18:54
-- Versione del server: 10.1.28-MariaDB
-- Versione PHP: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `masterpiada`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `addresses`
--

CREATE TABLE `addresses` (
  `username` varchar(30) NOT NULL,
  `street` varchar(30) NOT NULL,
  `house_num` int(10) UNSIGNED NOT NULL,
  `cap` char(5) NOT NULL,
  `city` varchar(30) NOT NULL,
  `info` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `addresses`
--

INSERT INTO `addresses` (`username`, `street`, `house_num`, `cap`, `city`, `info`) VALUES
('Mario', 'Via Roma', 123, '47521', 'Cesena', 'Mario Rossi'),
('utente001', 'Via Viona', 321, '47521', 'Cesena', '');

-- --------------------------------------------------------

--
-- Struttura della tabella `admins`
--

CREATE TABLE `admins` (
  `username` varchar(30) NOT NULL,
  `password` char(128) NOT NULL,
  `salt` char(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `admins`
--

INSERT INTO `admins` (`username`, `password`, `salt`) VALUES
('admin', '092d5fe48ce17ef98d7f557781dc9611446e4689edb70d132bc347b7576b31b80fb56f1f925202dd90172683ebf9b7f246d29c2b45c84389b10d88a35026f4f7', '9b7e8a56dabce14e864bbf21dbd591d84aeff8b6f85752bbba9f2a09dc4d66cbc1fa762e32fcc13aa08ee9aa3495c6b4f5679dcfb48935acc7ea48ac31b5be66');

-- --------------------------------------------------------

--
-- Struttura della tabella `login_attempts`
--

CREATE TABLE `login_attempts` (
  `username` varchar(30) NOT NULL,
  `time` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `login_attempts`
--

INSERT INTO `login_attempts` (`username`, `time`) VALUES
('Alex96', '1530463323');

-- --------------------------------------------------------

--
-- Struttura della tabella `members`
--

CREATE TABLE `members` (
  `username` varchar(30) NOT NULL,
  `name` varchar(30) NOT NULL,
  `surname` varchar(30) NOT NULL,
  `gender` char(1) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` char(128) NOT NULL,
  `salt` char(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `members`
--

INSERT INTO `members` (`username`, `name`, `surname`, `gender`, `email`, `password`, `salt`) VALUES
('Alex96', 'Alex', 'Piscaglia', 'm', 'ap@gmail.com', 'c6a7c94d41adc0f2279e072de31181954ffab843b3b3316490b313ab1e9097d784bf8ec62ca71a3723efd2922e6dd1f2d833ceb4f402b3da54acffd192ccf513', 'f38fb9b55bb175b2ca65ea68fc61e559c1b22305087289de5572fff02527cbfa2b3e4594ab6c1bba481feb52681224e9586874f35bea3337432474dad9458d00'),
('Mario', 'Mario', 'Rossi', 'm', 'mrossi@yahoo.it', 'b8c8541b0f61b62487396c744b6aaf919641257fce92783a7d963e3cffeb73bd2ac08c334552d44ea872e2a4ea8464e4a1eb63166dea80b5879628e6ed3cc4e5', '69b02f931a90fd6a133669df853f441ac7f276d336fe02d8cf11d1651f0ce061faa68f634f9fa3f2e2ec56f87bc4dbd3b6e18fbd09e5ca78a1cb4115d5009680'),
('utente001', 'Alice', 'Bianchi', 'f', 'alicebianchi96@gmail.com', 'dafab842cad69310d89c498c26d46ab0e0dd089749d37977a4722118e5f3ed88990e4859ef128df36f110e6672788c754f3679581f3a97cac9b34681a1a6e352', 'bcf9495b55c55eee07ac83eb19916fd926f863e6abe598441702791d90da776c6bbb6139f4fb466780cd631fe2a6fc8818a3ad1e68220b4d69d97bc45c8b7a3a');

-- --------------------------------------------------------

--
-- Struttura della tabella `notifications`
--

CREATE TABLE `notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `receiver` varchar(50) NOT NULL,
  `message` varchar(50) NOT NULL,
  `date` datetime NOT NULL,
  `received` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `notifications`
--

INSERT INTO `notifications` (`id`, `receiver`, `message`, `date`, `received`) VALUES
(1, 'admin', 'Nuovo ordine inserito', '2018-07-01 18:29:50', 1),
(2, 'admin', 'Nuovo ordine inserito', '2018-07-01 18:32:23', 1),
(3, 'admin', 'Nuovo ordine inserito', '2018-07-01 18:34:26', 1),
(4, 'Mario', 'Il tuo ordine è in preparazione', '2018-07-01 18:35:10', 1),
(5, 'Mario', 'Il tuo ordine è in consegna', '2018-07-01 18:35:14', 1),
(6, 'admin', 'Nuovo ordine inserito', '2018-07-01 18:36:20', 1),
(7, 'Alex96', 'Il tuo ordine è in preparazione', '2018-07-01 18:37:03', 1),
(8, 'Alex96', 'Il tuo ordine è in consegna', '2018-07-01 18:37:05', 1),
(9, 'Mario', 'Il tuo ordine è in preparazione', '2018-07-01 18:37:06', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `orders`
--

CREATE TABLE `orders` (
  `id` int(11) UNSIGNED NOT NULL,
  `username` varchar(30) NOT NULL,
  `datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `delivery` time NOT NULL,
  `address` varchar(100) DEFAULT NULL,
  `payment` varchar(20) NOT NULL,
  `amount` double UNSIGNED NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `orders`
--

INSERT INTO `orders` (`id`, `username`, `datetime`, `delivery`, `address`, `payment`, `amount`, `status`) VALUES
(13, 'Alex96', '2018-07-01 18:29:50', '13:00:00', 'Via Sacchi, 3, 47521 Cesena', 'PayPal', 5.5, 'In consegna'),
(14, 'Mario', '2018-07-01 18:32:23', '12:30:00', 'Via Roma, 123, 47521 Cesena - Mario Rossi', 'Carta di credito', 10.2, 'Consegnato'),
(15, 'utente001', '2018-07-01 18:34:26', '14:30:00', 'Via Genova, 145, 47521 Cesena', 'Contanti', 13.5, 'Pendente'),
(16, 'Mario', '2018-07-01 18:36:20', '13:00:00', 'Via Roma, 123, 47521 Cesena - Mario Rossi', 'Contanti', 3, 'In preparazione');

-- --------------------------------------------------------

--
-- Struttura della tabella `orders_details`
--

CREATE TABLE `orders_details` (
  `order_id` int(11) UNSIGNED NOT NULL,
  `product_id` int(11) UNSIGNED NOT NULL,
  `quantity` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `orders_details`
--

INSERT INTO `orders_details` (`order_id`, `product_id`, `quantity`) VALUES
(13, 7, 1),
(13, 12, 1),
(14, 6, 1),
(14, 8, 1),
(14, 9, 1),
(15, 5, 2),
(15, 11, 1),
(16, 7, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `products`
--

CREATE TABLE `products` (
  `id` int(11) UNSIGNED NOT NULL,
  `type` varchar(30) NOT NULL,
  `name` varchar(50) NOT NULL,
  `ingredients` varchar(255) NOT NULL,
  `price` double UNSIGNED NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `products`
--

INSERT INTO `products` (`id`, `type`, `name`, `ingredients`, `price`, `deleted`) VALUES
(1, 'Piadina', 'Speck e gorgonzola', 'Speck, gorgonzola', 3.7, 0),
(2, 'Piadina', 'Crudo e squacquerone', 'Prosciutto crudo, squacquerone, rucola', 4.5, 0),
(3, 'Crescione', 'Classico', 'Pomodoro, mozzarella, prosciutto cotto', 2.9, 0),
(4, 'Crescione', 'Patate e salsiccia', 'Patate, salsiccia', 3.5, 0),
(5, 'Panino', 'Boscaiolo', 'Speck, funghi, fontina', 5, 0),
(6, 'Panino', 'Cotoletta', 'Cotoletta, ketchup, maionese', 2.2, 0),
(7, 'Bevanda', 'Acqua', '', 1, 0),
(8, 'Bevanda', 'Birra', '', 2, 0),
(9, 'Crescione', 'Erbe', 'Spinaci, formaggio', 4, 0),
(10, 'Bevanda', 'Coca Cola', '', 1.5, 0),
(11, 'Bevanda', 'Sprite', '', 1.5, 0),
(12, 'Piadina', 'Cotto, fontina e rucola', 'Prosciutto cotto, fontina, rucola', 4.5, 0),
(13, 'Piadina', 'Salame', 'Salame', 3.2, 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `reviews`
--

CREATE TABLE `reviews` (
  `id` int(11) UNSIGNED NOT NULL,
  `username` varchar(30) NOT NULL,
  `date` date NOT NULL,
  `text` varchar(256) NOT NULL,
  `rating` int(4) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `reviews`
--

INSERT INTO `reviews` (`id`, `username`, `date`, `text`, `rating`) VALUES
(4, 'Alex96', '2018-07-01', 'Ordino cassoni, piadine e spesso anche patatine qui da anni, e posso dire che, dopo aver provato decine di altre piadinerie, questa è decisamente la migliore. ', 5),
(5, 'Mario', '2018-07-01', 'Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua.', 2),
(6, 'utente001', '2018-07-01', 'Ut enim ad minim veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur.', 4);

-- --------------------------------------------------------

--
-- Struttura della tabella `timetables`
--

CREATE TABLE `timetables` (
  `week_day` varchar(10) NOT NULL,
  `opening_time` time DEFAULT NULL,
  `closing_time` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `timetables`
--

INSERT INTO `timetables` (`week_day`, `opening_time`, `closing_time`) VALUES
('Domenica', NULL, NULL),
('Giovedì', '09:00:00', '17:00:00'),
('Lunedì', '09:00:00', '17:00:00'),
('Martedì', '09:00:00', '17:00:00'),
('Mercoledì', '09:00:00', '17:00:00'),
('Sabato', NULL, NULL),
('Venerdì', '09:00:00', '17:00:00');

-- --------------------------------------------------------

--
-- Struttura della tabella `universities`
--

CREATE TABLE `universities` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL,
  `street` varchar(50) NOT NULL,
  `num` int(10) UNSIGNED NOT NULL,
  `cap` char(5) NOT NULL,
  `city` varchar(50) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `universities`
--

INSERT INTO `universities` (`id`, `name`, `street`, `num`, `cap`, `city`, `deleted`) VALUES
(1, 'Ingegneria e scienze informatiche', 'Via Sacchi', 3, '47521', 'Cesena', 0),
(2, 'Ingegneria biomedica', 'Via Genova', 181, '47521', 'Cesena', 0),
(3, 'Scienze e tecniche psicologiche', 'Piazza Aldo Moro', 90, '47521', 'Cesena', 0),
(4, 'Ingegneria elettronica', 'Genova', 181, '47521', 'Cesena', 0),
(5, 'Architettura', 'Cavalcavia', 55, '47521', 'Cesena', 0);

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`username`);

--
-- Indici per le tabelle `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`username`);

--
-- Indici per le tabelle `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD KEY `username` (`username`);

--
-- Indici per le tabelle `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`username`);

--
-- Indici per le tabelle `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`);

--
-- Indici per le tabelle `orders_details`
--
ALTER TABLE `orders_details`
  ADD PRIMARY KEY (`order_id`,`product_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indici per le tabelle `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`);

--
-- Indici per le tabelle `timetables`
--
ALTER TABLE `timetables`
  ADD PRIMARY KEY (`week_day`);

--
-- Indici per le tabelle `universities`
--
ALTER TABLE `universities`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT per la tabella `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT per la tabella `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT per la tabella `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT per la tabella `universities`
--
ALTER TABLE `universities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `addresses`
--
ALTER TABLE `addresses`
  ADD CONSTRAINT `addresses_ibfk_1` FOREIGN KEY (`username`) REFERENCES `members` (`username`);

--
-- Limiti per la tabella `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD CONSTRAINT `login_attempts_ibfk_1` FOREIGN KEY (`username`) REFERENCES `members` (`username`);

--
-- Limiti per la tabella `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`username`) REFERENCES `members` (`username`);

--
-- Limiti per la tabella `orders_details`
--
ALTER TABLE `orders_details`
  ADD CONSTRAINT `orders_details_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `orders_details_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Limiti per la tabella `reviews`
--
ALTER TABLE `reviews`
  ADD CONSTRAINT `reviews_ibfk_1` FOREIGN KEY (`username`) REFERENCES `members` (`username`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
